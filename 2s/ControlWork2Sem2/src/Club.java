public class Club {
    private String clubName;
    private int idC;
    private int clubYear;
    private String city;

    public Club(String clubName, int idC, int clubYear, String city) {
        this.city = city;
        this.clubName = clubName;
        this.idC = idC;
        this.clubYear = clubYear;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }

    public int getClubYear() {
        return clubYear;
    }

    public void setClubYear(int clubYear) {
        this.clubYear = clubYear;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String toString() {
        return
    }
}
