public class Gamer {
    private String fio;
    private int idG;
    private String amplua;
    private String country;
    private int gamerYear;

    public Gamer (String fio, int idG, String amplua, String country, int gamerYear) {
        this.fio = fio;
        this.idG = idG;
        this.amplua = amplua;
        this.country = country;
        this.gamerYear = gamerYear;

    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public int getIdG() {
        return idG;
    }

    public void setIdG(int idG) {
        this.idG = idG;
    }

    public String getAmplua() {
        return amplua;
    }

    public void setAmplua(String amplua) {
        this.amplua = amplua;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getGamerYear() {
        return gamerYear;
    }

    public void setGamerYear(int gamerYear) {
        this.gamerYear = gamerYear;
    }
}
