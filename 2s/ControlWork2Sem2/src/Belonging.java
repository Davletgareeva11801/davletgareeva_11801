public class Belonging {

    private Club club;
    private Gamer gamer;
    private int duration;
    private int yearContract;

    public Belonging(Club club, Gamer gamer, int duration, int yearContract) {
        this.club = club;
        this.gamer = gamer;
        this.duration = duration;
        this.yearContract = yearContract;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Gamer getGamer() {
        return gamer;
    }

    public void setGamer(Gamer gamer) {
        this.gamer = gamer;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getYearContract() {
        return yearContract;
    }

    public void setYearContract(int yearContract) {
        this.yearContract = yearContract;
    }

    public String toString() {
        return
    }
}
