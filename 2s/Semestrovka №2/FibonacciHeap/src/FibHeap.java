import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class FibHeap {
    private Node min = null;
    private int size = 0;

    public Node enqueue(int value, int priority) {
        checkPriority(priority);
        Node result = new Node(value, priority);
        min = mergeLists(min, result);
        ++size;
        return result;
    }

    public Node getMin() {
        if (isEmpty())
            throw new NoSuchElementException("Heap is empty.");
        return min;
    }

    public boolean isEmpty() {
        return min == null;
    }

    public int getSize() {
        return size;
    }

    public static FibHeap merge(FibHeap one, FibHeap two) {

        FibHeap result = new FibHeap();
        result.min = mergeLists(one.min, two.min);
        result.size = one.size + two.size;

        one.size = two.size = 0;
        one.min = null;
        two.min = null;
        return result;
    }

    public Node dequeueMin() {
        if (isEmpty())
            throw new NoSuchElementException("Heap is empty.");
        --size;
        Node minKey = min;
        if (min.right == min) {
            min = null;
        } else {
            min.left.right = min.right;
            min.right.left = min.left;
            min = min.right;
        }
        if (minKey.child != null) {

            Node curr = minKey.child;
            do {
                curr.parent = null;

                curr = curr.right;
            } while (curr != minKey.child);
        }
        min = mergeLists(min, minKey.child);


        if (min == null) return minKey;
        List<Node> treeTable = new ArrayList<Node>();
        List<Node> toVisit = new ArrayList<Node>();
        for (Node curr = min; toVisit.isEmpty() || toVisit.get(0) != curr; curr = curr.right)
            toVisit.add(curr);
        for (Node curr : toVisit) {

            while (true) {

                while (curr.degree >= treeTable.size())
                    treeTable.add(null);
                if (treeTable.get(curr.degree) == null) {
                    treeTable.set(curr.degree, curr);
                    break;
                }
                Node other = treeTable.get(curr.degree);
                treeTable.set(curr.degree, null);
                Node minimum = (other.priority < curr.priority) ? other : curr;
                Node max = (other.priority < curr.priority) ? curr : other;

                max.right.left = max.left;
                max.left.right = max.right;
                max.right = max.left = max;
                minimum.child = mergeLists(minimum.child, max);
                max.parent = min;

                max.mark = false;

                ++minimum.degree;

                curr = min;
            }
            if (curr.priority <= min.priority) min = curr;
        }
        return minKey;
    }

    public void decreaseKey(Node entry, double newPriority) {
        checkPriority(newPriority);
        if (newPriority > entry.priority)
            throw new IllegalArgumentException("New priority exceeds old.");

        decreaseKeyUnchecked(entry, newPriority);
    }

    public void delete(Node entry) {

        decreaseKeyUnchecked(entry, -1);

        dequeueMin();
    }

    private void checkPriority(double priority) {
        if (Double.isNaN(priority))
            throw new IllegalArgumentException(priority + " is invalid.");
    }

    private static Node mergeLists(Node one, Node two) {

        if (one == null && two == null) {
            return null;
        } else if (one != null && two == null) {
            return one;
        } else if (one == null && two != null) {
            return two;
        } else {
            Node oneNext = one.right;
            one.right = two.right;
            one.right.left = one;
            two.right = oneNext;
            two.right.left = two;


            return one.priority < two.priority ? one : two;
        }
    }

    private void decreaseKeyUnchecked(Node entry, double priority) {

        entry.priority = priority;
        if (entry.parent != null && entry.priority <= entry.parent.priority)
            cutNode(entry);
        if (entry.priority <= min.priority)
            min = entry;
    }

    private void cutNode(Node entry) {

        entry.mark = false;

        if (entry.parent == null) return;

        if (entry.right != entry) {
            entry.right.left = entry.left;
            entry.left.right = entry.right;
        }

        if (entry.parent.child == entry) {

            if (entry.right != entry) {
                entry.parent.child = entry.right;
            }

            else {
                entry.parent.child = null;
            }
        }
        --entry.parent.degree;
        entry.left = entry.right = entry;
        min = mergeLists(min, entry);
        if (entry.parent.mark)
            cutNode(entry.parent);
        else
            entry.parent.mark = true;

        entry.parent = null;

    }


    public Node search(int priority) {
        if (min.getPriority() == priority) return min;
        else {
            return search1(priority, min);
        }
    }

    public Node search1(double priority, Node startingNode) {
    if (startingNode.getPriority() == priority) return startingNode;
        Node finish = null;
        while (startingNode.right != startingNode && finish == null) {
            if (startingNode.right.priority == priority) {
                finish = startingNode.right;
            }

            if (startingNode.child != null && startingNode.degree >= 2) {
                finish = search1(priority, startingNode.child);
            } else {
                if (startingNode.child != null && startingNode.degree == 1) {
                    if (startingNode.child.priority == priority) {
                        finish = startingNode.child;
                    }
                }
                startingNode = startingNode.right;
            }
        }

        return finish;
    }
}