public class Node {

    private int key;      // ключ
    Node parent;  // указатель на родительский узел
    Node child;   // указатель на один из дочерних узлов
    Node left;    // указатель на левый узел того же предка
    Node right;   // указатель на правый узел того же предка
    int degree;  // степень вершины
    boolean mark;
    double priority;


    public String toString() {
        return "value: " + this.getKey();
    }

    public int getKey() {
        return key;
    }

    public double getPriority() {
        return priority;
    }
    public Node(int key, double priority) {
        left = right = this;
        this.key = key;
        this.priority = priority;
    }
}