import com.sun.javafx.geom.Edge;

import java.io.File;
//import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("data.txt"));
        while (sc.hasNextLine()) {
            String[] jar = sc.nextLine().split("\t");
            int n = jar.length;
            int[] array = new int[n];
            for (int i = 0; i < n; i++) {
                array[i] = Integer.parseInt(jar[i]);
            }
            FibHeap H1 = new FibHeap();
            for (int i = 0; i < array.length; i++) {
                H1.enqueue(array[i], i);
            }
            //System.out.println();
            Node node = H1.search(9);

            long c1 = System.nanoTime();
            H1.delete(node);
            long c2 = System.nanoTime();
            //System.out.println(/*"numberOfElements: " +*/ array.length);


            System.out.println(/*"Time:" +*/ (c2 - c1));

            //System.out.println("\t");*/
        }


    }
}