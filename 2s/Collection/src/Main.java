import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {

    public static void main (String[] args) {
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student("Shakirova", 2001, "Chelyabinsk"));
        students.add(new Student("Bady", 2000, "Kyzyl"));
        students.add(new Student("Khismatova", 2001, "Dusyanovo"));
        Collections.sort(students, new Comparator<Student>(){
            public int compare (Student s1, Student s2){
                //return s1.getYear()-s2.getYear();
                return s1.getCity().compareTo(s2.getCity());
            }
        }
        /*Collections.sort(students, (s1,s2)-> {
                    return s1.getCity().compareTo(s2.getCity());
                }
        );*/
        System.out.println(students);
    }
}