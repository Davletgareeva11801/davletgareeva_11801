public class Elem<T> {

    T value;
    Elem<T> next;

    public Elem<T>() {
        this(0,null);
    }

    public Elem<T>(int value, Elem<T> next) {
        this.value = value;
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public Elem<T> getNext() {
        return next;
    }


}
