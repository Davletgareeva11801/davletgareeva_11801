public interface Collection<T> extends Iterable <T>{
    boolean add(T x);
    void remove(Object x);
    int size();
    boolean isEmpty();
    boolean contains(Object o);
    Object[] toArray();
    void clear();
}