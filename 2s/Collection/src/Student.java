import java.util.Comparator;

public class Student implements Comparable<Student> {
    private String fio;
    private Integer year;
    private String city;

    public Student(String fio, Integer year, String city){
        this.fio = fio;
        this.year = year;
        this.city = city;
    }
    public String toString() {
        return fio + " " + year + " " + city;
    }

    public String getFio() {
        return fio;
    }

    public int getYear() {
        return year;
    }

    public String getCity() {
        return city;
    }

    public int compareTo(Student x) {
        return this.fio.compareTo(x.getFio());
    }
}