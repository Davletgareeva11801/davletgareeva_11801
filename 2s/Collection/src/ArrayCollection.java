import java.util.Collection;
import java.util.Iterator;

public class ArrayCollection <T> implements Collection<T> {
    private int size = 0 ;
    private T[] array;

    public ArrayCollection() {
        array = (T[]) new Object[0];
    }


    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayIterator<T>(array);
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(T x) {
        try {
            T[] temp = array;
            array = (T[]) new Object[temp.length + 1];
            System.arraycopy(temp, 0, array, 0, temp.length);
            array[array.length - 1] = x;
            return true;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean remove(int index) {
        T[] temp = array;
        array = (T[]) new Object(temp.length - 1);
        System.arraycopy(temp, 0, array, 0, index);
        int numMoved = temp.length - index - 1;
        System.arraycopy(temp, index + 1, array, index, numMoved);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}