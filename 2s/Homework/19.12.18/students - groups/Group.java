public class Group {
	
	private String name;
	private int course;
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name + ":" + course + "course";
	}

	public int getCourse() {
		return course;
	}

	public Group(String name, int course) {
		this.name = name;
		this.course = course;
	}
	
	
}