import java.io.*;
import java.util.*;

public class Main {
	
	public static void main (String[] args)
		throws IOException{
			//открыть файл
			int i = 0;
			Scanner sc = new Scanner(new File("Groups.txt"));
			//узнать кол-во из файла
			int numberOfGroups = Integer.parseInt(sc.nextLine());
			System.out.println(numberOfGroups);
			//создать массив
			Group [] groups = new Group[numberOfGroups];
			while (sc.hasNextLine()) {
				
				String [] data = sc.nextLine().split("\t");
				//создать объект
				//добавить его в массив
				groups[i] = new Group(data[0], Integer.parseInt(data[1]));
				i++;
			}
			System.out.println(Arrays.toString(groups));
			
			int j = 0;
			Scanner sc1 = new Scanner(new File("Students.txt"));
			int numberOfStudents = Integer.parseInt(sc1.nextLine());
			System.out.println(numberOfStudents);
			Student [] students = new Student[numberOfStudents];
			while (sc1.hasNextLine()) {
				String [] data1 = sc1.nextLine().split("\t");
				boolean flag = false;
				//System.out.println("'" + data1[3] + "'");
				for (int m = 0; m < numberOfGroups && !flag; m++) {
					//System.out.println("* '" + groups[m].getName() + "'");
					if (data1[3].equals(groups[m].getName())) {
						//System.out.println(groups[m]);
						flag = true;
						students[j] = new Student(data1[0], data1[1], Integer.parseInt(data1[2]), groups[m]);
					}
					
				}
				j++;
			}
			
			//System.out.println(j);
			//System.out.println(Arrays.toString(students));
			
			//№1
			String nameOfGroup = "11-801";
			printStudentsByGroup(nameOfGroup, students);
			
			
			//№2
			printCountOfStudents(students, groups);
			

			//№3
			int courseOfGroup = 2;
			printStudentOfNCourse(courseOfGroup, students);
			
			
			//№4
			System.out.print("freshmen from Kazan: ");
			printFreshmenFromKazan(students);
			
			//№5
			int presentYear = 2018;
			System.out.print("adult sophomores from Kazan: ");
			printAdultSophomoresFromKazan(students, presentYear);
			
		}
		
		public static void printStudentsByGroup(String nameOfGroup,Student[] students){
			for (int i = 0; i < students.length; i++) {
				if (students[i].getGroup().getName().equals(nameOfGroup)){
					System.out.println(students[i].getSurname());
				}
			}
		}
		public static void printCountOfStudents(Student[] students, Group[] groups){
			for (int j = 0; j < groups.length; j++){
			System.out.print("number of students in the group № " + groups[j].getName() + ": " );	
				int count = 0;
				for (int i = 0; i < students.length; i++) {
					if (students[i].getGroup().getName().equals(groups[j].getName())){
						count++;
					}
				}
			System.out.println(count);
			}
		}
		public static void printStudentOfNCourse(int courseOfGroup, Student[] students){
			for (int i = 0; i < students.length; i++) {
				if (students[i].getGroup().getCourse() == courseOfGroup){
					System.out.println(students[i].getSurname());
				}
			}
		}
		public static void printFreshmenFromKazan(Student[] students){
			for (int i = 0; i < students.length; i++) {
				if ((students[i].getGroup().getCourse() == 1) && (students[i].getCity().equals("Kazan"))) {
					System.out.println( students[i].getSurname());
				}
			}
		}
		public static void printAdultSophomoresFromKazan(Student[] students, int presentYear){
			for (int i = 0; i < students.length; i++) {
				if ((students[i].getGroup().getCourse() == 2) && (students[i].getCity().equals("Kazan")) && (presentYear - students[i].getYear() >= 18)) {
					System.out.println(students[i].getSurname());
				}
			}
		}
		
}
