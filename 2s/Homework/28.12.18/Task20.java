/**
* @author Alsu Davletgareeva
* 11-801
* Task 20
*/

public class Task20 {
	public static int maximum(int [] array) {
		int max = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}
	
	public static void main(String[] args) {
        int[][] arrayX2 = {{1, 2, 3}, {1, 2, 3}, {0, 7, 2} };
		
		
		int sum1 = arrayX2[0][0] + arrayX2[1][1] + arrayX2[2][2];
		int sum2 = arrayX2[0][2] + arrayX2[1][0] + arrayX2[2][1];
		int sum3 = arrayX2[0][1] + arrayX2[1][2] + arrayX2[2][0];
		int[] arrayX1 = {sum1, sum2, sum3};
		System.out.println(maximum(arrayX1));
	}
}
		