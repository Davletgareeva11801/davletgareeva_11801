/**
* @author Alsu Davletgareeva
* 11-801
* Task 21
*/

public class Task21 {
	
	public static boolean checkSum(int[][] matrix) {
		int[] array = sumDiagonal(matrix);
		for (int i = 0; i < array.length; i++) {
			if (array[i] % 2 != 0) {
				return false;
			}
		}
		return true;
	}
	
	public static int[] sumDiagonal(int[][] matrix) {
		int[] array = new int[matrix.length];
		int new_n = 0;
		
			for (int i = 0; i <= matrix.length; i++) {
				for(int j = 0; j < matrix[i].length; j++) {
					if (i == matrix[i].length - j - 1) {
						array[i] += matrix[i][j];
					}
				
				}
			}
			return array;
	}
				
	public static void main(String[] args) {
		
		int[][] arrayX2 = {{1, 2, 3}, {1, 2, 3}, {0, 7, 2} };
		
		System.out.println(checkSum(arrayX2));
	}
}