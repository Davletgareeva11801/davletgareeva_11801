/**
* @author Alsu Davletgareeva
* 11-801
* Task 26
*/

public class Task26 {
	
	public static boolean hasLineMultipleOf3(int[][][] matrix3d) {
		
		for(int i = 0; i < matrix3d.length; i++) {       
			for(int j = 0; j < matrix3d[i].length; j++) {    
				for(int k = 0; k < matrix3d[i][j].length; k++) {
					if (matrix3d[i][j][k] % 3 != 0) {
						return false;
					}
				}
			
			}
		
		}
	return true;
	}
					   
	public static void main(String[] args) {
		 int[][][] matrix3d = {{{32, -3, 27}, {3, 3, 12}}, {{7, 9, 6}, {3, 3, 3}}};
		 System.out.println(hasLineMultipleOf3(matrix3d));
	}
}
		 