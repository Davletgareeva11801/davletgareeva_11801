/**
* @author Alsu Davletgareeva
* 11-801
* Task 17
*/

public class Task17 {
	
	public static void main (String[] args) {
		int a = -12;
		int b = 3;
		
		int temp = 0;
		int x = 0;
		while (temp + Math.abs(b) <= Math.abs(a)) {
			temp += Math.abs(b);
			x++;
		}
		
		int remainder = Math.abs(a) - temp;
		if ((a < 0 && b > 0) || (a > 0 && b < 0)) {
			x *= -1;
			remainder *= -1;
		}
		
		System.out.println(x + " " + remainder);
	}
}

	
	
	