/**
* @author Alsu Davletgareeva
* 11-801
* Task 25
*/

public class Task25 {
	
	public static int[][] multiplyByMatrix(int[][] m1, int[][] m2) {
       int m1ColLength = m1[0].length; // m1 columns length
       int m2RowLength = m2.length;    // m2 rows length
       if(m1ColLength != m2RowLength) {
		   return null; // matrix multiplication is not possible
	   }
       int mRRowLength = m1.length;    // m result rows length
       int mRColLength = m2[0].length; // m result columns length
       int[][] mResult = new int[mRRowLength][mRColLength];
       for(int i = 0; i < mRRowLength; i++) {         // rows from m1
           for(int j = 0; j < mRColLength; j++) {     // columns from m2
               for(int k = 0; k < m1ColLength; k++) { // columns from m1
                   mResult[i][j] += m1[i][k] * m2[k][j];
               }
           }
       }
       return mResult;
    } 
	public static void main(String[] args) {
		
		int[][] matrix1 = {{2, -3}, {4, -6}};
		int[][] matrix2 = {{9, -6}, {6, -4}};
		int[][] matrixResult = multiplyByMatrix(matrix1,matrix2);
		for (int i = 0; i < matrixResult.length; i++) {
            System.out.println();
			for (int j = 0; j < matrixResult[i].length; j++) {
                System.out.print(matrixResult[i][j]+" ");
            }
        }
	
	}
}
		