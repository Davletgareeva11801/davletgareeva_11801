/**
* @author Alsu Davletgareeva
* 11-801
* Task 28
*/

public class Task28 {
	
	
	public static boolean compare(String str1, String str2) {
		if (str1.length() <= str2.length()) {
			int shortest = str1.length();
		}else{
			int shortest = str2.length();
		}
		
		for (int i = 0; i < shortest.length(); i++) {
			if (str1.charAt(i) > str2.charAt(i)) {
				return true;
			}
		}
		return false;
	}
	public static void sort(String[] array){
		for (int i = array.length - 1; i > 0; i--) {
			for (int j = 0; j <= i; j++) {
				if (compare(array[j], array[j + 1])){
					String temp = array[i];
					array[i] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		String[] array = {"bac", "abcd", "bca"};
		String[] result = sort(array);
		for (int i = 0; i < result.length; i++) {
			System.out.println(result[i]);
		}
	}
}
	
	
		