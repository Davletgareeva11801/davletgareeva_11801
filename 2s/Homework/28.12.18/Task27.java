/**
* @author Alsu Davletgareeva
* 11-801
* Task 27
*/

public class Task27 {
	
	public static int compare(String str1, String str2) {
		int x = 1;
		int y = -1;
		for (int i = 0; i < str1.length(); i++) {
			if (str1.charAt(i) < str2.charAt(i)) {
				return x;
			}
		}
			return y;
	}
			
	public static void main (String[] args) {
		String str1 = "abfc";
		String str2 = "aafc";
		System.out.println(compare(str1, str2));
	}
}
		
	