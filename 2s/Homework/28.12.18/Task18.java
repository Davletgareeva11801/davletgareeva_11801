/**
* @author Alsu Davletgareeva
* 11-801
* Task 18
*/

public class Task18 {
	
	public static int multiply(int a, int b){
		
		int result = 0;
		for(int i = 0; i < Math.abs(b); i++) {
			result += Math.abs(a);
		}
		
		if ((a < 0)&(b > 0)||(a > 0)&(b < 0)) {
			return (-1)*result;
		}
		
		return result;
	}
			
		
	public static void main (String[] args) {
		int a = -2;
		int b = 3;
		System.out.println(multiply(a,b));
	}
	}
	