/**
* @author Alsu Davletgareeva
* 11-801
* Task 22
*/

public class Task22 {
	
	public static void main (String[] args) {
		
		int[][] matrix = {{1, 2, 3, 4, 5}, {1, 2, 3, 7, 4}, {0, 7, 2, 1,4},{1, 2, 3, 7, 4}, {0, 7, 2, 1,7}};
		int a = 0;
		int b = matrix[1].length - 1;
		
		for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
				if ((j > (a + i))&&(j < (b - i))) {
					matrix[i][j] = 0;
					if (i < (matrix[i].length - 1)/2) {
					matrix[matrix[i].length - 1 - i][j] = 0;
					}
				}
			}
		}
        for (int i = 0; i < matrix.length; i++) {
            System.out.println();
			for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+" ");
            }
        }
	}
}