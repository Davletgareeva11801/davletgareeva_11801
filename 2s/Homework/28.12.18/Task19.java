/**
* @author Alsu Davletgareeva
* 11-801
* Task 19
*/

public class Task19 {
	
	public static int multiply(int digit, int number){
		
		int result = 0;
		for(int i = 0; i < Math.abs(digit); i++) {
			result += Math.abs(number);
		}
		
		if ((number < 0)&(digit > 0)||(number > 0)&(digit < 0)) {
			return (-1)*result;
		}
		return result;
	}
			
	public static void main (String[] args) {
		int digit = -2;
		int number = 36;
		System.out.println(multiply(digit,number));
	}
}