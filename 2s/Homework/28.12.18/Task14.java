/**
* @author Alsu Davletgareeva
* 11-801
* Task 14
*/

import java.util.Scanner;

public class Task14 {
	
	public static void main (String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] array = new int[n+1];
		array[0] = 1;
		array[1] = -3;
		for (int i = 1; i <= n-2; i+=2){
			array[i+1] = array[i - 1] + 4;
			array[i+2] = array[i] - 4;
		}
		for (int i = 0; i < n; i++){
			System.out.println(array[i]);
		}
	}
}