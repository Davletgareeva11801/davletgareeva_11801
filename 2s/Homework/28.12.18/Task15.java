/**
* @author Alsu Davletgareeva
* 11-801
* Task 15
*/

import java.util.Scanner;

public class Task15 {
	
	public static int power(int a, int b){
		int result = 1;
		for (int i=1; i<=b; i++){
			result = result * a;
		}
		return result;
		
	}
	public static int getCountsOfDigits(int number) {
        int count = (number == 0) ? 1 : 0;
        while (number != 0) {
            count++;
            number /= 10;
        }
        return count;
    }
	public static void main (String[] args){
		Scanner input = new Scanner(System.in); 
		System.out.println("Enter array length: ");
		int size = input.nextInt(); 
		int array[] = new int[size]; 
		System.out.println("Insert array elements:");
		
		for (int i = 0; i < size; i++) {
			array[i] = input.nextInt(); 
		}
		int sum = 0;
			for (int i = 0; i < size; i++) {
				sum = sum + getCountsOfDigits(array[i]);
			}
		int result1 = 0;
		int x = 0;
		
			for (int i = 0; i < size; i++) {
				x = x + getCountsOfDigits(array[i]); 
				result1 = result1 + array[i] * power(10, sum - x);
				
			}
		int result2 = 0;
		int h = 0;		
			for (int i = size-1; i >= 0 ; i--) {
				h = h + getCountsOfDigits(array[i]); 
				result2 = result2 + array[i] * power(10, sum - h);
			}
			System.out.println(result1);
			System.out.println(result2);
			
	}
}
			
	