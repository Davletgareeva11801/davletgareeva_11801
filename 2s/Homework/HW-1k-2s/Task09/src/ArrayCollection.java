/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 09
 */

import java.util.Collection;
import java.util.Iterator;

public class ArrayCollection<T> implements Collection<T> {
    private final int CAPACITY = 1000;
    private Object[] array = new Object[CAPACITY];
    private int size;
    @Override
    public int size() {
        return size();
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < array.length; i++){
            if(array[i].equals(o)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayIterator((T[])array, size);
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) array;
    }

    @Override
    public boolean add(T t) {
        array[size] = t;
        size ++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for(int i = 0; i < size; i++) {
           if(array[i].equals(o)){
               for(int j = i; j < (size + 1); j++){
                   array[j] = array[j+1];
               }
               size--;
               return true;
           }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Collection<T> coll = (Collection<T>)c;
        for(T t : coll){
            if(!this.contains(t)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for(T t: c){
            this.add(t);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Collection<T> coll = (Collection<T>)c;
        for(Object o: array){
            if(coll.contains(o)){
                this.remove(o);

            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Collection<T> coll = (Collection<T>)c;
        for(Object o: array){
            if(!coll.contains(o)){
                this.remove(o);

            }
        }
        return true;
    }

    @Override
    public void clear() {

    }
}
