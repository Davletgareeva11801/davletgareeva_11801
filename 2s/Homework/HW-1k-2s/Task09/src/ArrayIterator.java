import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {

    private int index = 0;
    private T[] values;
    private int size;
    public ArrayIterator(T[] values, int size) {
        this.values = values;
        this.size = size;
    }
    @Override
    public boolean hasNext() {
        return index + 1 < size;
    }
    @Override
    public T next() {
        return (T) values[index++];
    }
}
