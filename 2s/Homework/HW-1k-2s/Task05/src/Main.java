/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 05
 */

public class Main {
    public static int[] a = {1,2};

    public static void main (String[] args) {
        Elem p = null;
        Elem head = null;
        for(int i = 0; i < a.length; i++) {
            p = new Elem(a[i], head);
            head = p;
        }
        p = Elem.deletePenultimate(head);
        while(p != null){
            System.out.println(p.value);
            p = p.next;
        }
    }
}