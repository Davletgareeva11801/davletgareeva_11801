public class Elem {
    int value;
    Elem next;

    public Elem() {
        this(0, null);
    }

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public static Elem deletePenultimate(Elem h) {
        Elem p = h;
        if ((p.next == null)||(p == null)) {
            return h;
        }
        if (p.next.next == null) {
            return h.next;
        }
        while(p.next.next.next != null) {
            p = p.next;
        }
        p.next = p.next.next;
        return h;
    }
}