/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 04
 */

public class Main {
    public static int[] a = {1,2,17,-9,32};

    public static void main (String[] args) {
        Elem p = null;
        Elem head = null;
        for(int i = 0; i < a.length; i++) {
            p = new Elem(a[i], head);
            head = p;
        }
        p = Elem.deleteLast(head);
        while(p != null){
            System.out.println(p.value);
            p = p.next;
        }
    }
}
