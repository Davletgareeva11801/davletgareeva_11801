public class Elem {
    int value;
    Elem next;

    public Elem() {
        this(0, null);
    }

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public static Elem deleteLast(Elem head) {
        Elem p = head;
        if ((p.next == null) || (p == null)) {
            return null;
        } else {
            while (p.next.next != null){
                p = p.next;
            }
            p.next = null;
        }
        return head;
    }
}