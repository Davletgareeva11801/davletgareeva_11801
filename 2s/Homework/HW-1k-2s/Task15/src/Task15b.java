// Task 15b)
import java.util.Stack;

public class Task15b {
    Stack<Character> st = new Stack<>();
    boolean error = false;
    public boolean correct(String s){
        for (int i = 0; (i < s.length())&&(!error); i++){
            char c = s.charAt(i);
            if(c == '(' || c == '{' || c == '[' ){
                st.push(c);
            }else{
                char top = st.pop();
                if ((c == ')' && top != '(')||(c == '}' && top != '{')||(c == ']' && top != '[')){
                    error = true;
                }
            }
        }
        return(!error);
    }
}