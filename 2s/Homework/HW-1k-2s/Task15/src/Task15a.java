/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 15a)
 */
public class Task15a {
    public static String correct(String s){
        int balance = 0;
        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (c == '('){
                balance++;
            } else {
                if(c == ')'){
                    balance--;
                }
            }
            if(balance < 0){
                return "incorrectly";
            }
        }
        if(balance == 0){
            return "correctly";
        }else{
            return "incorrectly";
        }
    }
    public static void main(String[] args){
        System.out.println(correct(")("));
    }

    
}
