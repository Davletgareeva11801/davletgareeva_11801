public class Elem {
    int value;
    Elem next;

    public Elem() {
        this(0, null);
    }

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public static Elem deleteFirstElemK(Elem head, int k) {
        Elem p = head;
        boolean flag = false;
        if (p.value == k) {
            return p.next;
        } else {
            while ((flag != true)&&(p.next != null)){
                if(p.next.value == k){
                    p.next = p.next.next;
                    flag = true;
                }
                p = p.next;
            }
        }
        return head;
    }
}