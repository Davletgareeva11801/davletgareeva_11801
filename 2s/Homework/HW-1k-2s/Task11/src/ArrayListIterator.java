
import java.util.ListIterator;

public class ArrayListIterator<T> implements ListIterator<T> {

    private int index;
    private T[] array;
    private int size;
    public ArrayListIterator(T[] array, int size) {
        this.array = array;
        this.size = size;
        index = 0;
    }

    @Override
    public boolean hasNext() {
        return index < size;
    }

    @Override
    public T next() {
        //index++;
        return (T)array[index++];

    }

    @Override
    public boolean hasPrevious() {
        return index > 0;
    }

    @Override
    public T previous() {
        return (T)array[index--];
    }

    @Override
    public int nextIndex() {
        return index + 1;
    }

    @Override
    public int previousIndex() {
        return index - 1;
    }

    @Override
    public void remove() {
        for(int i = index; i < size - 1; i++){
            array[i] = array[i + 1];
        }
        size--;
    }

    @Override
    public void set(T t) {
        array[index] = t;
    }

    @Override
    public void add(T t) {
        for(int i = size; i >= index + 1; i--){
            array[i] = array[i-1];
        }
        array[index] = t;
    }


}
