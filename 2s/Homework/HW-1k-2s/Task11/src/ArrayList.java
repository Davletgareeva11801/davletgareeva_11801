/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 11
 */

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayList<T> implements List<T> {
    private int size;
    private final int CAPACITY = 10000;
    Object [] array = new Object[CAPACITY];


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for(int i = 0; i < size; i++){
            if(array[i].equals(o)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator<T>((T[]) array, size);
    }

    @Override
    public Object[] toArray() {
        return (T[])array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[])array;
    }

    @Override
    public boolean add(T t) {
        array[size] = t;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for(int i = 0; i < size; i++){
            if (array[i].equals(o)){
                for(int j = i; i < size - 1; j++){
                    array[j] = array[j + 1];
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Collection<T> coll = (Collection<T>)c;
        for(T t : coll){
            if(!this.contains(t)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for(T t : c){
            this.add(t);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        int i = 0;
        for(T t : c){
            this.add(index + 1, t);
            i++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for(Object t : c){
            if(!this.contains(t)){
                return false;
            }else{
                this.remove(t);
            }

        }
        return true;
    }


    @Override
    public boolean retainAll(Collection<?> c) {
        for(T t : (T[])array){
            if(!c.contains(t)){
                this.remove(t);
            }
        }
        return true;
    }


    @Override
    public void clear() {

    }

    @Override
    public T get(int index) {
        return (T)array[index];
    }

    @Override
    public T set(int index, T element) {
        T oldElement = (T) array[index];
        array[index] = element;
        return oldElement;
    }

    @Override
    public void add(int index, T element) {
        for(int i = size; i >= index + 1; i--){
            array[i] = array[i-1];
        }
        array[index] = element;
        size++;
    }

    @Override
    public T remove(int index) {
        T elem = (T)array[index];
        for(int i = size; i >= index + 1; i--){
            array[i] = array[i+1];
        }
        size--;
        return elem;
    }

    @Override
    public int indexOf(Object o) {
        for(int i = 0; i < size; i++) {
            if(array[i].equals(o)){
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int cur = -1;
        for(int i = 0; i < size; i++) {
            if(array[i].equals(o)){
                 cur = i;
            }
        }
        return cur;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new ArrayListIterator<T>((T[])array, size);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        /*Object arr[] = new Object [size - index];
        for (int i = 0; i < size - index; i++){
            arr[i] = array[index];
            index++;
        }*/
        return this.subList(index, size - 1).listIterator();
    }

    public ArrayList(T[] array){
        int i = 0;
        for(Object o : array){
            this.array[i] = o;
            i++;
        }
        size = array.length;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        Object [] newArray = new Object[toIndex - fromIndex + 1];
        for (int i = 0; i < newArray.length; i++){
            newArray[i] = array[fromIndex];
            fromIndex++;
        }
        return new ArrayList(newArray);
    }
}
