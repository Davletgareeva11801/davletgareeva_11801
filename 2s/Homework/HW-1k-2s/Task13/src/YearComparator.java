import java.util.Comparator;

public class YearComparator implements Comparator<Student> {
    @Override
    public int compare(Student s1, Student s2) {
        return s1.getYear() - s2.getYear();
    }
}
