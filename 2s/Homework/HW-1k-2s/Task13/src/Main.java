/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 13
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Student> students = new ArrayList<Student>();
        Scanner sc = new Scanner(new File("students.txt"));
        while (sc.hasNextLine()) {
            String[] newStudent = sc.nextLine().split("\t");
            students.add(new Student(newStudent[0], Integer.parseInt(newStudent[1]), newStudent[2], Integer.parseInt(newStudent[3])));
        }
        /*Collections.sort(students, new AverageScoreComparator());
        System.out.println(students);
        Collections.sort(students, new FioComparator());
        System.out.println(students);*/
        Collections.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getCity().compareTo(o2.getCity());
            }
        });
        System.out.println(students);
        Collections.sort(students, (s1, s2)
                -> {
            return s1.getFio().compareTo(s2.getFio());
        });
        System.out.println(students);
    }
}