public class Student {
    private String fio;
    private int year;
    private String city;
    private int averageScore;

    public Student(String fio, int year, String city, int averageScore){
        this.averageScore = averageScore;
        this.city = city;
        this.fio = fio;
        this.year = year;
    }

    public String getFio() {
        return fio;
    }

    public int getYear() {
        return year;
    }

    public String getCity() {
        return city;
    }

    public int getAverageScore() {
        return averageScore;
    }
    public String toString(){
        return "name: " + this.fio + " " + "year: " + " " + this.year + " " + "city: " + this.city + " average score: " + this.averageScore;
    }
}
