public class Elem {
    int value;
    Elem next;

    public Elem() {
        this(0, null);
    }

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static Elem InsertBeforeAfterK(Elem head, int m, int k) {
        if (head.value == k) {
            Elem prev = new Elem(m, head);
            Elem next = new Elem(m, head.next);
            head.next = next;
            head = prev;
        }
        Elem p = head.next;
       while (p.next != null) {

            if (p.next.value == k) {
                Elem prev = new Elem(m, p.next);
                p.next = prev;
                p = prev.next;
                Elem next = new Elem(m, p.next);
                p.next = next;
            }
            p = p.next;
        }
        return head;
    }
}