/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 08
 */

public class Main {
    public static int[] a = {11,1,9,11,25,32,11};

    public static void main (String[] args) {
        Elem p = null;
        Elem head = null;
        for(int i = 0; i < a.length; i++) {
            p = new Elem(a[i], head);
            head = p;
        }
        p = Elem.InsertBeforeAfterK(head, 17, 11);
        while(p != null){
            System.out.println(p.value);
            p = p.next;
        }
    }
}