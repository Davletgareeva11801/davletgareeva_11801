public class Elem {
    int value;
    Elem next;

    public Elem() {
        this(0,null);
    }

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public Elem getNext() {
        return next;
    }
}