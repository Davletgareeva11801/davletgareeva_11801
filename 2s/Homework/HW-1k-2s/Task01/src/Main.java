/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 01
 */

import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Elem p = null;
        Elem head = null;
        int size = 4;
        for(int i = 0; i < size; i++) {
            p = new Elem();
            p.value = in.nextInt();
            p.next = head;
            head = p;
        }
        p = head;
        while (p != null) {
            System.out.println(p.value);
            p = p.next;
        }
    }
}