public class Elem {
    int value;
    Elem next;

    public Elem() {
        this(0, null);
    }

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public static Elem deleteAllElemK(Elem head, int k) {
        while (head.value == k) {
            if (head.next != null) {
                head = head.next;
            } else {
                return null;
            }
        }
        Elem p = head;
        while (p.next != null) {
            if (p.next.value != k) {
                p = p.next;
            }else{
                p.next = p.next.next;
            }

        }
        return head;
    }
}
