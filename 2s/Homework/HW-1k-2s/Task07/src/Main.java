/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 07
 */

public class Main {
    public static int[] a = {17,1,2,17,17,32,17};

    public static void main (String[] args) {
        Elem p = null;
        Elem head = null;
        for(int i = 0; i < a.length; i++) {
            p = new Elem(a[i], head);
            head = p;
        }
        p = Elem.deleteAllElemK(head, 17);
        while(p != null){
            System.out.println(p.value);
            p = p.next;
        }
    }
}