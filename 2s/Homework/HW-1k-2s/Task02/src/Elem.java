public class Elem {
    int value;
    Elem next;

    public Elem() {
        this(0, null);
    }

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public static int findMax(Elem head) {
        int max = head.value;
        Elem p = head.next;
        while(p != null) {
            if(p.value > max) {
                max = p.value;
            }
            p = p.next;
        }
        return max;
    }

    public static boolean hasNegative(Elem head) {
        boolean flag = false;
        Elem p = head;
        while((flag == false)&& (p != null)) {
            if (p.value < 0) {
                flag = true;
            }
            p = p.next;
        }
        return flag;
    }

    public static int sum (Elem head) {
        Elem p = head;
        int sum = 0;
        while(p != null) {
            sum += p.value;
            p = p.next;
        }
        return sum;
    }
}