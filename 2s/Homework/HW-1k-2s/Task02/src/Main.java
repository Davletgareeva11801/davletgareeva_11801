/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 02
 */

import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Elem p = null;
        Elem head = null;
        int size = 6;
        for (int i = 0; i < size; i++) {
            p = new Elem();
            p.value = in.nextInt();
            p.next = head;
            head = p;
        }
        p = head;
        System.out.println(Elem.findMax(p));
        System.out.println(Elem.hasNegative(p));
        System.out.println(Elem.sum(p));
    }
}
