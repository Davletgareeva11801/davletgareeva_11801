/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 14
 */
public class LinkedStack<T> implements MyStack<T> {
    private Elem<T> head;
    private int size;
    @Override
    public void push(T t) {
        Elem<T> p = head;
        while(p.getNext() != null){
            p = p.getNext();
        }
        p.setNext(new Elem<T>(t, null));
    }

    @Override
    public T pop() {
        Elem<T> p = head;
        while(p.getNext().getNext() != null){
            p = p.getNext();
        }
        T elem = p.getNext().getValue();
        p.setNext(null);
        return elem;
    }

    @Override
    public T peek() {
        Elem<T> p = head;
        while(p.getNext() != null){
            p = p.getNext();
        }
        return p.getValue();
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}
