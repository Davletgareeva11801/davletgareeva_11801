public class ArrayStack<T> implements MyStack<T> {

    private final static int CAPACITY = 1000;
    private Object[] array = new Object[CAPACITY];
    private int size;

    public ArrayStack(T[] array, int size){
        if (array == null){
            this.size = 0;
        }
        for (int i = 0; i < array.length; i++){
            this.array[i] = array[i];
        }
        this.size = size;
    }
    @Override
    public void push(T t) {
        array[size] = t;
        size++;
    }

    @Override
    public T pop() {
        if(this == null || this.isEmpty()){
            return null;
        }
        size--;
        return (T)array[size];
    }

    @Override
    public T peek() {
        if (this == null || this.isEmpty()){
            return null;
        }
        return (T)array[size - 1];
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}
