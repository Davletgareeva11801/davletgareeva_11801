/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 16
 */

import java.util.Stack;

public class Task16 {

    public static Stack<Double> st = new Stack<>();

    public static double calculate(String postfix) {
        String[] pf = postfix.split(", ");
        for (String s : pf) {
            if (!(s.equals("*") || s.equals("+") || s.equals("-") || s.equals("/"))) {
                st.push(Double.parseDouble(s));
            } else {
                switch (s) {
                    case "+":
                        st.push(st.pop() + st.pop());
                        break;
                    case "-":
                        st.push(st.pop() * (-1) + st.pop());
                        break;
                    case "*":
                        st.push(st.pop() * st.pop());
                        break;
                    case "/":
                        st.push((1 / st.pop()) * st.pop());
                }
            }
        }
        return st.peek();
    }
    public  static void main(String[] args){
        System.out.println(calculate("10, 15, +, 20, *"));
    }
}


