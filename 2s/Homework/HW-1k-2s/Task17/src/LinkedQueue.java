import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class LinkedQueue<T> implements Queue<T> {
    private Elem<T> head;
    @Override
    public int size() {
        int result = 0;
        Elem<T> p = head;
        while(p != null){
            p = p.getNext();
            result++;
        }
        return result;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(T t) {
        head = new Elem<>(t, head);
        return true;
    }

    @Override
    public T remove() {
        if(this.isEmpty()){
            throw new NullPointerException();
        }
        T t = head.getValue();
        head = head.getNext();
        return t;
    }

    @Override
    public T poll() {
        T t = head.getValue();
        head = head.getNext();
        return t;
    }

    @Override
    public T element() {
        if(this.isEmpty()){
            throw new NullPointerException();
        }
        return head.getValue();
    }

    @Override
    public T peek() {
        return head.getValue();
    }
}
