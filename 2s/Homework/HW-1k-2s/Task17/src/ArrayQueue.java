import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 17
 */

public class ArrayQueue<T> implements Queue<T> {
    private final static int CAPACITY = 1000;
    private Object[] array = new Object[CAPACITY];
    private int size;
    private int cur;
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(T t) {
        array[size] = t;
        size++;
        return true;
    }

    @Override
    public T remove() {
        if(this.isEmpty()){
            throw new NullPointerException();
        }
        T result = (T)array[cur];
        cur++;
        if(cur == size - 1){
            array[0] = array[cur];
            cur = 0;
        }
        return result;
    }

    @Override
    public T poll() {
        T result = (T)array[0];
        cur++;
        if(cur == size - 1){
            array[0] = array[cur];
            cur = 0;
        }
        return result;
    }

    @Override
    public T element() {
        if(this.isEmpty()){
            throw new NullPointerException();
        }
        return (T)array[cur];
    }

    @Override
    public T peek() {
        return (T)array[cur];
    }
}
