import java.util.Iterator;

public class LinkedIterator<T> implements Iterator<T> {

    private Elem<T> head;
    private Elem<T> cur;

    public LinkedIterator(Elem<T> head){
        this.head = head;
        cur = this.head;
    }

    public T next(){
        T result = cur.getValue();
        cur = cur.getNext();
        return result;
    }

    public boolean hasNext(){
        return cur != null;
    }

}

