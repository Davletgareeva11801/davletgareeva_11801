/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 10
 */

import java.util.Collection;
import java.util.Iterator;

public class LinkedCollection<T> implements Collection<T> {
    private Elem head;
    private int size;

    public LinkedCollection(Elem<T> head){
        this.head = head;
        int s = 0;
        Elem<T> p = head;
        while(p != null){
            s++;
            p = p.getNext();
        }
        this.size = s;
    }
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head.equals(null);
    }

    @Override
    public boolean contains(Object o) {
        Elem<T> p = head;
        while(p != null){
            if(p.getValue().equals(o)){
                return true;
            }
            p = p.getNext();
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedIterator<T>(head);
    }

    @Override
    public T[] toArray() {
        Object[] result = new Object[size];
        Elem<T> p = head;
        int i = 0;
        while(p != null){
            result[i]= p.getValue();
            p = p.getNext();
        }
        return (T[])result;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[])this.toArray();
    }

    @Override
    public boolean add(Object o) {
        Elem<T> p = head;
        while(p.getNext() != null){
            p = p.getNext();
        }
        p.setNext(new Elem<T>((T)o, null));
        size++;
        return true;
    }


    @Override
    public boolean remove(Object o) {
        Elem<T> p = head;
        while(p.getNext() != null){
            if(p.getNext().getValue().equals(o)){
                p.setNext(p.getNext().getNext());
                return true;
            }
            p = p.getNext();
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Collection<T> coll = (Collection<T>)c;
        for(T t : coll){
            if(!this.contains(t)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for(T t : c){
            this.add(t);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Collection<T> coll = (Collection<T>)c;
        Elem<T> p = head;
        while(p != null){
            if(c.contains(p.getValue())){
                this.remove(p.getValue());
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Collection<T> coll = (Collection<T>)c;
        Elem<T> p = head;
        while(p != null){
            if(!c.contains(p.getValue())){
                this.remove(p.getValue());
            }
        }
        return true;
    }

    @Override
    public void clear() {

    }
}
