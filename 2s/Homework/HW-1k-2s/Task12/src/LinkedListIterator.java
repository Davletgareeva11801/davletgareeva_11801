import java.util.ListIterator;

public class LinkedListIterator<T> implements ListIterator<T> {
    private Elem<T> head;
    private Elem<T> cur;
    private int size;

    public LinkedListIterator(Elem<T> head, int size){
        this.size = size;
        this.head = head;
        this.cur = head;
    }
    @Override
    public boolean hasNext() {
        return head.getIndex(cur) < size;
    }

    @Override
    public T next() {
        T result = cur.getValue();
        cur = cur.getNext();
        return result;
    }

    @Override
    public boolean hasPrevious() {
        return head.getIndex(cur) > 0;
    }

    @Override
    public T previous() {
        T prev = cur.getValue();
        cur = head.prev(cur);
        return null;
    }

    @Override
    public int nextIndex() {
        return head.getIndex(cur) + 1;
    }

    @Override
    public int previousIndex() {
        return head.getIndex(cur) - 1;
    }

    @Override
    public void remove() {
        head.prev(cur).setNext(cur.getNext());
        cur = cur.getNext();
        size--;
    }

    @Override
    public void set(T t) {
        cur.setValue(t);
    }

    @Override
    public void add(T t) {
        head.prev(cur).setNext(new Elem<T>(t, cur));
        cur = head.prev(cur);
    }
}
