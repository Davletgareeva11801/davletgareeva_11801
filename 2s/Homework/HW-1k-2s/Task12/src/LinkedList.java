/**
 * @author Alsu Davletgareeva
 * 11-801
 * Task 12
 */
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList<T> implements List<T> {
    private T cur;
    private Elem head;
    private int size;

    public LinkedList(Elem<T> head, int size){
        this.head = head;
        this.size = size;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Elem<T> p = head;
        while (p != null){
            if(p.getValue().equals(o)){
                return true;
            }
            p = p.getNext();
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator<>(head, size);
    }

    @Override
    public Object[] toArray() {
        T res[] = (T[])new Object[size];
        Elem<T> p = head;
        int i = 0;
        while(p != null){
            res[i] = p.getValue();
            i++;
            p = p.getNext();
        }
        return res;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] res = (T1[])(new Object[size]);
        Elem<T1> p = (Elem<T1>) head;
        int i = 0;
        while(p != null){
            res[i] = p.getValue();
            i++;
            p = p.getNext();
        }
        return res;
    }

    @Override
    public boolean add(T t) {
        Elem<T> p = head;
        while(p.getNext() != null){
            p = p.getNext();
        }
        p.setNext(new Elem<>(t, null));
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Elem<T> p = head;
        if(p.getValue().equals(o)){
            head = p.getNext();
            return true;
        }
        while(p != null){
            if(p.getNext().getValue().equals(o)){
                p.setNext(p.getNext().getNext());
                size--;
                return true;
            }
            p = p.getNext();
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for(T t : (Collection<T>) c){
            if(!this.contains(t)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for(T t : c){
            this.add(t);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        int i = 0;
        for(T t : c){
            this.add(index + i, t);
            i++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for(T t : (Collection<T>)c){
            if(!this.contains(t)){
                return false;
            }else{
                this.remove(t);
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {//удаляет все,кроме данной collection
        for(T t : this){
            if(!c.contains(t)){
                this.remove(t);
            }
        }
        return true;
    }

    @Override
    public void clear() {

    }

    @Override
    public T get(int index) {
        Elem<T> p = head;
        int i = 0;
        while(i < index){
            p = p.getNext();
            i++;
        }
        return p.getValue();
    }

    @Override
    public T set(int index, T element) {
        Elem<T> p = head;
        int i = 0;
        while(i < index){
            p = p.getNext();
            i++;
        }
        T elem = p.getValue();
        p.setValue(element);
        return elem;
    }

    @Override
    public void add(int index, T element) {
        Elem<T> p = head;
        int i = 0;
        while(i < index - 1){
            p = p.getNext();
            i++;
        }
        p.setNext(new Elem<T>(element, p.getNext()));
        size++;
    }

    @Override
    public T remove(int index) {
        Elem<T> p = head;
        int i = 0;
        while(i < index - 1){
            p = p.getNext();
            i++;
        }
        T element = p.getNext().getValue();
        p.setNext(p.getNext().getNext());
        return element;
    }

    @Override
    public int indexOf(Object o) {
        int i = -1;
        Elem<T> p = head;
        while(p != null){
            i++;
            if(p.getValue().equals(o)){
                return i ;
            }
            p = p.getNext();
        }
        return i;
    }

    @Override
    public int lastIndexOf(Object o) {
        int lastIndex = -1;
        int i = 0;
        Elem<T> p = head;
        while(p != null){
            if(p.getValue().equals(o)){
                lastIndex = i;
            }
            i++;
            p = p.getNext();
        }
        return lastIndex;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new LinkedListIterator<T>(head, size);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return this.subList(index, size - 1).listIterator();
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        int i = 0;
        Elem<T> p = head;
        while(i < fromIndex){
            i++;
            p = p.getNext();
        }
        return new LinkedList<T>(p, toIndex - fromIndex + 1);
    }
}
