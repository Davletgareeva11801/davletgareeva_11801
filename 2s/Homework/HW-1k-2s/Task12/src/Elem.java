public class Elem<T> {
    private T value;
    private Elem<T> next;

    public Elem(T value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Elem<T> getNext() {
        return next;
    }

    public void setNext(Elem<T> next) {
        this.next = next;
    }

    public int getIndex(Elem<T> elem){
        Elem<T> p = this;
        int i = 0;
        while(p != null){
            if(p.equals(elem)){
                return i;
            }
            i++;
            p = p.getNext();
        }
        return -1;
    }

    public Elem<T> prev(Elem<T> elem){
        Elem<T> p = this;
        while(p.next != null){
            if(p.next == elem){
                return p;
            }
            p = p.getNext();
        }
        return null;
    }
}
