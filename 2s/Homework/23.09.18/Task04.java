/**
* @author Alsu Davletgareeva
* 11-801
* Task 04 
*/

import java.util.Scanner;
 
public class Task04 {
    public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int r = in.nextInt();
		for (int y = -r; y <= r; y++){
			for (int x = -r; x <= r; x++){
				System.out.print((x*x + y*y) <= r*r ? "0" : "*");
			}
			System.out.println();
		}
	}
}
        
            