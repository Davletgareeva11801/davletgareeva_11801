/**
* @author Alsu Davletgareeva
* 11-801
* Task 02 
*/
import java.util.Scanner;

class Task02 {
    public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("k=");
		int k = in.nextInt();
		System.out.print("m=");
		int m  = in.nextInt();
		int a, b;
		if (k > m) {
			a = k;
			b = m+1;
		} else {
			a = m;
			b = k+1;
		}
		while ( a != b ) {			
			if ((b < a) & ((b)%3 == 0)) {
				System.out.println(b);
			}
			b++;
		}
	}
}
