/**
* @author Alsu Davletgareeva
* 11-801
* Task 06
*/
public class Task06 {
		
		public static void main (String[] args){
			
		Scanner in = new Scanner(System.in);
		System.out.print("x=");
		int x = in.nextInt();
		
			final double EPS = 1e-9;
			double s, a;
			int i;
			
			a = x;
			s = x;
			i = 1;
			while ((a > 0 ? a : -a ) > EPS) {			
				a = (a * (-1)* x * x) / (2 * i * (2 * i + 1));
		        s += a;
				i += 1;
			}
			System.out.println(s);
			
		
		}
	}