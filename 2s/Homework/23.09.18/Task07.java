/**
* @author Alsu Davletgareeva
* 11-801
* Task 07
*/
public class Task07 {
		
		public static void main (String[] args){
			
		Scanner in = new Scanner(System.in);
		System.out.print("x=");
		int x = in.nextInt();
		
			final double EPS = 1e-9;
			double s, a;
			int i;
			
			a = 1;
			s = 1;
			i = 0;
			while ((a > 0 ? a : -a ) > EPS) {			
				a = (a * (-1)* x * x) / ((i + 1) * (i + 2));
		        s += a;
				i += 2;
			}
			System.out.println(s);
			
		
		}
	}