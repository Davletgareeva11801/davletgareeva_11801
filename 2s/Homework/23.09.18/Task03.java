/**
* @author Alsu Davletgareeva
* 11-801
* Task 03 
*/
import java.util.Scanner;
 
public class Task03 {
    public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("n=");
		int n = in.nextInt();
		

		final double EPS = 1e-6;
		double x = 1;
		while (true) {
		double nx = (x + n / x) / 2;
		if (Math.abs(x - nx) < EPS)  break;
		x = nx;
		}
		

System.out.println(String.format("%.7g%n",x));
	}
}