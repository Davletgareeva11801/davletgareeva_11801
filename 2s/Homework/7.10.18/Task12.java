/**
* @author Alsu Davletgareeva
* 11-801
* Task 12 
*/


public class Task12 {
    public static void main(String[] args) {
		
		int[] a = {6, 80, 38, 1};
		int n = a.length;
		boolean p = false;
		int i = 0;
			while(!p && i < n - 1){
				if ((a[i] % 2 == 0) && (a[i] > a[i + 1]) && (a[i] > a[i - 1])){
					p = true;
				}	
					i++;
			}
			System.out.print(p);
	}
}
		