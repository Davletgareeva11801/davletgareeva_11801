/**
* @author Alsu Davletgareeva
* 11-801
* Task 09
*/


public class Task09 {
    public static void main(String[] args) {
		
		int[] a = {6, 882, 38, 77752, 2, 3, 24, 1};
		int n = a.length;
		boolean p = false;
		
		
		int count = 0;
		
			for(int i = 0; (!p & (count <= 3) & (i < n)) ; i++){
				int m = a[i];
				int size = 0;
				while (m!=0) {
					size++;
					m /= 10;
				}
				int k = 0;
				int b = 0;
					if((size == 3) || (size == 5)){
						k = a[i] % 2;
						b = a[i] / 10;
						while(b > 0 && b % 2 == k){
							b /= 10;
						}
						if(b == 0){
							count++;
						}
					}
			}
			if(count == 2){
				p = true;
			}
			System.out.print(p);
	}
}
			
				
			