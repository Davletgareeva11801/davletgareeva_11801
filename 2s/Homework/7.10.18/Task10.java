/**
* @author Alsu Davletgareeva
* 11-801
* Task 10
*/

public class Task10 {
    public static void main(String[] args) {
		
		int[] a = {68, 333, 33, 88, 672};
		int n = a.length;
		boolean p = false;
		int k = 0;
		int count = 0;
		
			for(int i = 0; (!p & (count < 4) & (i < n)) ; i++){
				int m = a[i];
				int size = 0;
				while (m!=0) {
					size++;
					m /= 10;
				}
				int j = 0;
				int[] b = new int [size];
				m = a[i];
				while ((j < size) & (m != 0)){
					b[j] = m % 10;
					m/=10;
					j++;
				}
				int j = 0;
				
				while (j < b.length-1){
				if (b[j] < b[j+1]){
					j++;
				}
				}
					
				if (j == b.length-2){
					count++;
					}
				
			}
			if (count == 3){
				p=true;
			}
			System.out.println(p);
				
}
}
			
				
			