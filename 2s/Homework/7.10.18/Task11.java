/**
* @author Alsu Davletgareeva
* 11-801
* Task 11 
*/
import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int k = in.nextInt();
		int n = in.nextInt();
		int m = n;
		int size = 0;
		while (m != 0){
			size++;
			m/=10;
		}
		int x = 0;
		int y = 1;
		int[] a= new int[size];
		for (int i = 0 ; i < size; i++) {
			a[i] = n % 10;
			if (i == 0) {
				y = 1;
			} else {
				y *=k;
			}
			x = x + a[i]*y;
			n /= 10;
		}
		System.out.println(x);
	}
}
