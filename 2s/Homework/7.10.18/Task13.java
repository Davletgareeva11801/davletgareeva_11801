/**
* @author Alsu Davletgareeva
* 11-801
* Task 13 
*/


public class Task13 {
    public static void main(String[] args) {
		
		int[] a = {6, 81, 38, 70, 2, 3, 24, 1};
		int n = a.length;
		boolean p = false;
		int i = 0;
		int k = 0;
			while(!p && (k <= 3) && (i < n - 1)){
				if ((a[i] % 2 == 0) && (a[i] > a[i + 1]) && (a[i] > a[i - 1])){
					k++;
				}	
				i++;
			}
				
			if (k == 2) {
					p = true;
			}
			System.out.print(p);
	}
}
		