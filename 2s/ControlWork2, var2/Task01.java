public class Task01 {
	
	public static int maximum(int [] array) {
		int max = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}
	public static int minimum (int [] array) {
		int min = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}
	
	public static void main (String [] args) {
		int [] array = new int[] {2,0,6,7,3};
		int max = maximum(array);
		System.out.println(max);
		int min = minimum(array);
		System.out.println(min);
		if (max > array.length) {
			max = array.length - 1;
		}
		for (int i = 0; i < max/2; i++) {
			int x = array[min + i];
			array[min + i] = array[max - i];
			array[max - i] = x;
		}
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
	}
}
			
			
	