import java.util.Scanner;

public class Task02 {
    public static  void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int numOfChair = sc.nextInt();
        int numOfTable = sc.nextInt();
        int numOfLifts = 0;
        numOfLifts += numOfTable / 2;
        if (numOfTable % 2 != 0){
            numOfLifts++;
            numOfChair--;
        }
        numOfLifts += numOfChair/3;
        if(numOfChair % 3 != 0){
            numOfLifts++;
        }
        System.out.println(numOfLifts);

    }
}
