import java.util.Scanner;

//var№1
public class Task01 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int sum = 0;
        sum += n * 3;
        int i = 0;
        while (n > 0 && i < m) {
            n -= 2;
            i++;
        }
        sum += n / 2 + n % 2;

        System.out.println(sum);
    }
}


