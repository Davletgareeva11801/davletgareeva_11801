public class Sphere implements Volumeable {
	
	private double r;
	
	public Sphere(double r) {
		this.r = r;
	}

	public double getS() {
		return 4 * Math.PI * r * r;
	}

	public double getV() {
		return (double)(4/3) * Math.PI * r * r * r;
	}
}
	
	
	

	