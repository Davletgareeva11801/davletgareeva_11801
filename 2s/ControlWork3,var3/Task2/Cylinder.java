public class Cylinder implements Volumeable {
	private double h;
	private double r;
	
	public Cylinder(double h, double r) {
		this.h = h;
		this.r = r;
	}
	
	public double getS() {
		return 2 * Math.PI * r * (r + h);
	}
	
	public double getV() {
		return Math.PI * r * r * h;
	}
}