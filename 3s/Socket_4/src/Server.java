import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class Server {
    public static CopyOnWriteArrayList<Connection> clients = null;
    public static void main(String[] args) throws IOException, InterruptedException {
        clients = new CopyOnWriteArrayList<Connection>();
        final int PORT = 1234;
        ServerSocket serverSocket = new ServerSocket(PORT);
        while (true) {
            Socket client = serverSocket.accept();
            Connection connection = new Connection(client);
            clients.add(connection);
            new Thread(connection).start();
            System.out.println("connected");
        }
    }
}