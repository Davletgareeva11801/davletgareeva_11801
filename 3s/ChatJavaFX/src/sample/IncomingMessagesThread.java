package sample;

import javafx.scene.control.TextArea;

import java.io.*;
import java.net.Socket;

public class IncomingMessagesThread extends Thread {
    private Socket socket;
    private TextArea ta;
    private BufferedReader bf;
    private PrintWriter pw;
    public IncomingMessagesThread(Socket socket) throws IOException {
        this.socket = socket;
        this.bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.pw = new PrintWriter(socket.getOutputStream(), true);
    }
    public void run(){
        while(true){
            try {
                //this.bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String message = bf.readLine();
            if (message != null){
                ta.appendText(message);
            }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
