package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Application {

    private String HOST = "localhost";
    private int PORT = 1234;
    private ServerSocket s;

    @Override
    public void start(Stage primaryStage) throws Exception{
        //socket = (new ServerSocket(PORT));
        Socket s = new Socket(HOST, PORT);

        //BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter pw = new PrintWriter(s.getOutputStream(),true);

        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Button btn = (Button) root.lookup("#btn_submit");
        TextField textField = (TextField) root.lookup("#message");
        TextArea textArea = (TextArea) root.lookup("#dialog");
        textArea.setEditable(false);


        btn.setOnAction(e -> {
                    textArea.appendText(textArea.getText() + "Server: " + textField.getText() + "\n");
                    //textField.setText("");
                    textField.clear();
                }
        );
        textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    textArea.appendText(/*textArea.getText() + */textField.getText() + "\n");
                    pw.println(textField.getText());
                    textField.clear();
                }
            }
        });
        IncomingMessagesThread thread = new IncomingMessagesThread(s);
        thread.run();
        primaryStage.setTitle("ChatServer");
        primaryStage.setScene(new Scene(root, 300, 275));
        //textField.
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}