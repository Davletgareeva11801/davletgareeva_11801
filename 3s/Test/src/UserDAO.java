import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO {
    static final String SQL_FIND_ALL = "SELECT * FROM users";
    static final String SQL_FIND_USER_BY_ID = "SELECT * FROM users WHERE id = ?";
    static final String SQL_ADD_USER = "INSERT INTO users (login, password) VALUES(?, ?)";
    static final String SQL_FIND_USER = "SELECT * FROM users WHERE login = ? AND password = ?";

    public User getUserById(String id) {
        try {
            PreparedStatement statement = ConnectionClass.getConnection().prepareStatement(SQL_FIND_USER_BY_ID);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return new User(id, rs.getString("login"), rs.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void add(User u) {
        PreparedStatement statement = null;
        try {
            statement = ConnectionClass.getConnection().prepareStatement(SQL_ADD_USER);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            statement.setString(1, u.getLogin());
            statement.setString(2, u.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean userIsExist(User u) {
        PreparedStatement statement = null;
        try {
            statement = ConnectionClass.getConnection().prepareStatement(SQL_FIND_USER);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            statement.setString(1, u.getLogin());
            statement.setString(2, u.getPassword());
            ResultSet rs = statement.executeQuery();
            return rs.next();
        } catch (SQLException e) {
           throw new IllegalArgumentException();
        }
    }
}