import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {
    protected  void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("current_user");
        if(user != null) {
            response.sendRedirect("/profile");
        } else {
            request.getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
            //generated form for enter, it will be return in response
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("current_user");
        UserDAO userDAO = new UserDAO();
        if(user != null) {
            response.sendRedirect("/profile");
        } else {
            if(userDAO.userIsExist(user)) {
                session.setAttribute("current_user", user);
                response.sendRedirect("/profile");
            } else {
                //error
                response.sendRedirect("/login");
            }
        }
    }
}