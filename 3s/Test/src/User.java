public class User {
    private String id;
    private String login;
    private String password;
    //private String userName;

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public User(String id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }
    public User(){
    }

    public String getId() {
        return id;
    }
}
