package our_reflection;

import our_reflection.C;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException{
        //создать экз всех классов на основе сеттингс
        //List instances = new ArrayList();
        Scanner sc = null;
        try {
            sc = new Scanner(new File("setting.dat"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        HashMap<String, Object> instances = new HashMap<>();//components
        while (sc.hasNextLine()){
            String className = sc.nextLine();
            try {
                Class c = Class.forName(className);
                Object o = c.newInstance(); //вызов дефолтного конструктора
                System.out.println(o);
                instances.put(className, o);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }

        }
        System.out.println(instances);

        //создать для каждого экз его атрибуты(на основе созданных на пред шаге объектов)
        for (String className: instances.keySet()){
            System.out.println(instances.keySet());
            Object o = instances.get(className);
            Class c = o.getClass();
            Field [] fields = c.getDeclaredFields(); //Declared - т.к нам нужны приватные поля

            for (Field field: fields){ //проходимся по всем полям конкретного класса
                field.setAccessible(true); //даем доступ на работу с приватным полем
                if(instances.containsKey(field.getType().getName())){
                    System.out.println("fieldName:    " + field.getType().getName());
                    try {
                        field.set(o, instances.get(field.getType().getName()));//соотв. полю(полю объекта o) присваиваем значение
                        System.out.println("className:    " + fields.getClass().getName());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

            }


        }
        C co = (C)instances.get("our_reflection.C");
        System.out.println(co.getPlayer());


    }
}
//параметризованный метод
