package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import java.lang.NumberFormatException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    @FXML TextField tfFirstNum;
    @FXML TextField tfSecondNum;
    @FXML TextField tfAnswer;


    public Button button;
    //public JTextField textField;
    public void hundleButtonAction(javafx.event.ActionEvent event){

        String firstNumber;
        String secondNumber;
        button = ((Button)event.getSource());
        firstNumber = tfFirstNum.getText();
        secondNumber = tfSecondNum.getText();
        String currentButtonPress = button.getText();
        System.out.println(currentButtonPress);
        calc(firstNumber, secondNumber, currentButtonPress);

    }

    public void calc(String firstNum, String secondNum, String action)throws NumberFormatException {
        try {
            Double num1 = Double.parseDouble(firstNum);
            Double num2 = Double.parseDouble(secondNum);

        } catch (NumberFormatException e) {
            //e.printStackTrace();
        }
        switch(action)
        {
            case "+":
                try {
                    tfAnswer.setText(String.valueOf(Double.parseDouble(firstNum) + Double.parseDouble(secondNum)));
                    break;
                } catch(NumberFormatException e) {
                    tfAnswer.setText("incorrect data entry");
                }

            case "-":
                tfAnswer.setText(String.valueOf(Double.parseDouble(firstNum) - Double.parseDouble(secondNum)));
                break;
            case "*":
                tfAnswer.setText(String.valueOf(Double.parseDouble(firstNum) * Double.parseDouble(secondNum)));
                break;
            case "/":
                tfAnswer.setText(String.valueOf(Double.parseDouble(firstNum) / Double.parseDouble(secondNum)));
                break;

        }


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
