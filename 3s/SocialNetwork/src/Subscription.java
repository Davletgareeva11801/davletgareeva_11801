public class Subscription {
    private String who;
    private String toWho;

    public Subscription(String who, String toWho){
        this.who = who;
        this.toWho = toWho;
    }

    public String getWho() {
        return who;
    }

    public String getToWho() {
        return toWho;
    }
    public String toString(){
        return "who: " + who + " " + "toWho: " + toWho;
    }
}
