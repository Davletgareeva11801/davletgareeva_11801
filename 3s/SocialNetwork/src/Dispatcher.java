import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class Dispatcher {
    public Map<String, String> getRequestParameters(String data) {
        Map<String, String> map = new HashMap<>();
        String[] pairs = data.split("&");
        for(int i = 0; i < pairs.length; i++){
            String[] tmp = pairs[i].split("=");
            map.put(tmp[0] ,tmp[1]);
        }
        return map;
    }

    public void process(String request) throws FileNotFoundException, ParseException {
        String [] requestData = request.split("\\?");
        //String path = requestData[0];
        Map<String, String> params = getRequestParameters(requestData[1]);
        if ("/friends".equals(requestData[0]) || request.contains("friends=true")){
            (new FriendsHandler()).service(params, request);
        }else if ("/im".equals(requestData[0]) ||
                  "/om".equals(requestData[0]) ||
                  "/om".equals(requestData[0])) {
            (new MessagesHandler()).service(params,request);
        }
        if("/profile".equals(requestData[0])){
            (new ProfileHandler()).service(params, request);
        }
    }
}