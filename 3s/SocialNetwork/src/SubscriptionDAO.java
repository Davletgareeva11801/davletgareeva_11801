import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class SubscriptionDAO {

    public static ArrayList<Subscription> subscriptions = new ArrayList<>();

    public ArrayList<Subscription> getSubscriptions() throws FileNotFoundException, ParseException {
        File file = new File("subscriptions.txt");
        Scanner scan = new Scanner(file);
        if (subscriptions.isEmpty()) {
            while (scan.hasNextLine()) {
                String[] str = scan.nextLine().split("\t");
                subscriptions.add(new Subscription(str[0], str[1]));
            }
        }
        return subscriptions;
    }
}