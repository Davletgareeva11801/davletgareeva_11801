import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {
    private String sendId;
    private String recId;
    private String text;
    private Date date;
    public String getSendId() {
        return sendId;
    }

    public String getRecId() {
        return recId;
    }

    public String getText() {
        return text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String data) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        date = format.parse(data);
    }
    public Message(String send_id, String rec_id, String data, String text) throws ParseException {
        this.sendId = send_id;
        this.recId = rec_id;
        this.text = text;
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        date = format.parse(data);
    }
}
