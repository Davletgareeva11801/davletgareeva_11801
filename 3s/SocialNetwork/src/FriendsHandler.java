import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

public class FriendsHandler implements Handler {
    public static ArrayList<Subscription> subs = new ArrayList<>();

    @Override
    public void service(Map<String, String> params, String path) throws FileNotFoundException, ParseException {
        User u = new UserDAO().getUserById(params.get("id"));
        subs = new SubscriptionDAO().getSubscriptions();
        ArrayList<User> friends = new ArrayList<>();
        //System.out.println(subs);
        for (Subscription s1 : subs) {
            if (s1.getWho().equals(u.getId())) {
                for (Subscription s2 : subs) {
                    if (s2.getWho().equals(s1.getToWho()) && s2.getToWho().equals(u.getId())) {
                        friends.add(new UserDAO().getUserById(s1.getToWho()));
                        break;
                    }
                }
            }
        }
        if (params.containsKey("friends")) {
            new Printer().printProfileWithFriends(u, friends);
        }
        new Printer().printFriends(friends);
    }
}
