import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MessagesHandler implements Handler {

    public static ArrayList<Message> messages = new ArrayList<>();

    @Override
    public void service(Map<String, String> params, String path) throws FileNotFoundException, ParseException {
        List<Message> chat = new ArrayList<>();
        if (path.startsWith("/messages")) {
            User u1 = new UserDAO().getUserById(params.get("id1"));
            User u2 = new UserDAO().getUserById(params.get("id2"));
            messages = new MessageDAO().getMessagesOfUser(u1);
            chat = messages.stream()
                    .filter(message ->
                            (message.getRecId().equals(u1.getId()) && message.getSendId().equals(u2.getId()))
                                    ||
                                    (message.getRecId().equals(u2.getId()) && message.getSendId().equals(u1.getId())))
                    .sorted((o1, o2) -> o1.getDate().compareTo(o2.getDate()))
                    .collect(Collectors.toList());

        }
        else if (path.startsWith("/im")) {
            User u = new UserDAO().getUserById(params.get("id"));
            messages = new MessageDAO().getMessagesOfUser(u);
            chat = messages.stream()
                    .filter(message ->
                            (message.getRecId().equals(u.getId())))
                    .sorted((o1, o2) -> o1.getDate().compareTo(o2.getDate()))
                    .collect(Collectors.toList());
        }
        else if (path.startsWith("/om")) {
            User u = new UserDAO().getUserById(params.get("id"));
            messages = new MessageDAO().getMessagesOfUser(u);
            chat = messages.stream()
                    .filter(message ->
                            (message.getSendId().equals(u.getId())))
                    .sorted((o1, o2) -> o1.getDate().compareTo(o2.getDate()))
                    .collect(Collectors.toList());
        }
        new Printer().printChat(chat);
    }
}
