import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class UserDAO {

    public static ArrayList<User> users = new ArrayList<>();

    public User getUserById(String id) throws FileNotFoundException {
        File file = new File("users.txt");
        Scanner scan = new Scanner(file);
        if (users.isEmpty()) {
            while (scan.hasNextLine()) {
                String[] str = scan.nextLine().split("\t");
                users.add(new User(str[0], str[1], str[2], Integer.parseInt(str[3])));
            }
        }
        for (User u  : users) {
            if (u.getId().equals(id)) {
                return u;
            }
        }
        return null;
    }
}
