import java.io.FileNotFoundException;
import java.util.Map;

public class ProfileHandler implements Handler{

    @Override
    public void service(Map<String, String> params, String path) throws FileNotFoundException {
        User u = new UserDAO().getUserById(params.get("id"));
        if (params.size() == 2 && params.get("friends").equals("true")) {

//            new Printer().printProfileWithFriends(u);
        }
        new Printer().printProfile(u);
    }
}
