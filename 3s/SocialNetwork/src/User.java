public class User {
    private String id;
    private String name;
    private String city;
    private int year;

    public User (String id, String name, String city, int year){
        this.id = id;
        this.name = name;
        this.city = city;
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String toString(){
        return "User name: " + this.name + " " + "city: " + this.city  + " year: " + this.year + " id: " + this.id;
    }
}
