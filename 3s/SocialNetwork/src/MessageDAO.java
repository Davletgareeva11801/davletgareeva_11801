import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class MessageDAO {

    public static ArrayList<Message> messages = new ArrayList<>();

    public ArrayList<Message> getMessagesOfUser(User user) throws FileNotFoundException, ParseException {
        File file = new File("messages.txt");
        Scanner scan = new Scanner(file);
        if (messages.isEmpty()) {
            while (scan.hasNextLine()) {
                String[] str = scan.nextLine().split("\t");
                messages.add(new Message(str[0], str[1], str[2], str[3]));
            }
        }
        ArrayList<Message> msgOfUser = new ArrayList<>();
        for (Message m : messages) {
            if (m.getRecId().equals(user.getId()) || m.getSendId().equals(user.getId())) {
                msgOfUser.add(m);
            }
        }
        return msgOfUser;
    }
}

