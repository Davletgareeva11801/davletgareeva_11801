import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Scanner;

public class Main {
    static Dispatcher d = new Dispatcher();

    public static void main(String[] args) throws FileNotFoundException, ParseException {
        boolean stop = false;
        Scanner scanner = new Scanner(System.in);

        while(!stop){
            String request = scanner.nextLine();
            if("exit".equals(request)){
                stop = true;
            }else {
                d.process(request);
            }
        }
    }
}