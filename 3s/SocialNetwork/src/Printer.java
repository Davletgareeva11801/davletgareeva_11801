import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class Printer {

    public void printProfile(User u) throws FileNotFoundException {
        File file = new File(System.currentTimeMillis() + " profile.html");
        PrintWriter pw = new PrintWriter(file);
        pw.println("<!DOCTYPE html>\n<html>\n\t<h1>" + u.getName() +
                "</h1>\n\t<h3> Город: " + u.getCity() +
                "</h3>\n\t<h3> Год рождения: " + u.getYear() +
                "</h3>\n</html>");
        pw.close();
    }

    public void printChat(List<Message> msgs) throws FileNotFoundException {
        File file = new File(System.currentTimeMillis() + " messages.html");
        PrintWriter pw = new PrintWriter(file);
        pw.println("<!DOCTYPE html>\n<html>");
        for (Message m : msgs) {
            pw.println("\t<p>"
                    + m.getDate() + "</p>\n\t<p>"
                    + new UserDAO().getUserById(m.getSendId()).getName()
                    + ": " + m.getText()
                    + "</p>");
        }
        pw.println("</html>");
        pw.close();
    }

    public void printFriends(List<User> friends) throws FileNotFoundException {
        File file = new File(System.currentTimeMillis() + " friends.html");
        PrintWriter pw = new PrintWriter(file);
        pw.println("<!DOCTYPE html>\n<html>");
        pw.println("\t<p>Друзья:");
        for (User f : friends) {
            pw.println("\t<p>"
                    + f.getName()
                    + "</p>");
        }
        pw.println("</html>");
        pw.close();
    }

    public void printProfileWithFriends(User u, List<User> friends) throws FileNotFoundException {
        File file = new File(System.currentTimeMillis() + " profile w fr.html");
        PrintWriter pw = new PrintWriter(file);
        pw.println("<!DOCTYPE html>\n<html>\n\t<h1>" + u.getName() +
                "</h1>\n\t<h3> Город: " + u.getCity() +
                "</h3>\n\t<h3> Год рождения: " + u.getYear() +
                "</h3>\n");
        pw.println("\t<h3>Друзья: </h3>");
        for (User f : friends) {
            pw.println("\t<p>"
                    + f.getName()
                    + "</p>");
        }
        pw.println("</html>");
        pw.close();
    }

}
