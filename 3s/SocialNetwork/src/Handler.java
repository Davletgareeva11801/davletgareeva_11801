import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Map;

public interface Handler {
    public void service(Map<String, String> params, String path) throws FileNotFoundException, ParseException;
}
