package sample;

import javafx.animation.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.awt.event.MouseEvent;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Pane root = new Pane();
        root.setOnMouseClicked(event -> {
        Rectangle r = new Rectangle(20, 20, Color.rgb(255,124, 255));
        int x = (int)((event).getX());
        int y = (int)((event).getY());
        r.setX(x);
        r.setY(y);
        root.getChildren().addAll(r);

        KeyValue kv = new KeyValue(r.xProperty(), 550);
        KeyFrame kf = new KeyFrame(Duration.seconds(3), kv) ;//е изм которые ты задал выполни за сек
        Timeline t = new Timeline(kf);
        //t.setAutoReverse(true);
        //t.setCycleCount(Animation.INDEFINITE);
        t.play();



        });
        Image i = new Image("kitty.jpg");
        ImageView iv = new ImageView();
        iv.setFitHeight(100);
        iv.setFitWidth(100);

        iv.setImage(i);
        iv.setX(10);
        iv.setY(10);
        root.getChildren().addAll(iv);

        iv.setOnMouseEntered(event -> {
            ScaleTransition st = new ScaleTransition(Duration.seconds(0.5), iv);

            st.setToX(1.5);
            st.setToY(1.5);
            FadeTransition ft = new FadeTransition(Duration.seconds(0.5), iv);
            ft.setToValue(1);
            ft.play();
            st.play();
        });

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 200, 200));
        primaryStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }
}
//несколько окон новый root, новый scene, fxml
