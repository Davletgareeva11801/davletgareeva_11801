import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.Random;

public class Server {
    public static void main(String[] args) throws IOException, InterruptedException {
        final int PORT = 1234;
        ServerSocket ss = new ServerSocket(PORT);
        while(true) {
            Socket s = ss.accept();
            System.out.println("new client connected");
            (new Thread(new Connection(s))).start();
        }
    }
}
