package paint;

import client.Message;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class PaintController  {
    @FXML
    private Canvas canvas;
    @FXML
    private ColorPicker colorPicker;
    @FXML
    private TextField brushSize;
    @FXML
    private CheckBox eraser;
    @FXML
    private Button clearButton;
    @FXML
    private Label playerName;
    @FXML
    private Label secretWord;

public void onExit(){
    Platform.exit();
}

public void initialize(){
    GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
    String name = Paint.name;
    System.out.println(name);
    playerName.setText(name);
    canvas.setOnMouseDragged(event -> {
        double size = Double.parseDouble(brushSize.getText());
        double x = event.getX() - size * 0.5;
        double y = event.getY() - size * 0.5;
        if(eraser.isSelected()){
            graphicsContext.clearRect(x,y,size,size);
        } else {
            graphicsContext.setFill(colorPicker.getValue());
            graphicsContext.fillRect(x,y,size,size);
        }
    });
    clearButton.setOnAction(event ->{
            graphicsContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    });
}


        @FXML
        private TextArea dialog;
        @FXML
        private TextField message;
        @FXML
        private Button send;
        @FXML
        ListView chatPane;


        public void setUsernameLabel(String name){
            this.playerName.setText(name);
        }
        public void setSecretWordLabel(String word){
            this.secretWord.setText(word);
        }

        public void sendButtonAction() throws IOException {
            String msg = message.getText();
            System.out.println("your message:" + msg);
            if(!message.getText().isEmpty()) {
                Listener.send(msg);
                message.clear();
            }else{
                System.out.println("no message");
            }
        }

        public void sendMethod(KeyEvent event) throws IOException{
            if(event.getCode()== KeyCode.ENTER){
                sendButtonAction();
            }
        }

    /*public void setUserList(String msg){
            Platform.runLater(() -> {
        String fullMessage =  dialog.getText();
        dialog.clear();
        dialog.appendText(fullMessage + msg + "\n");
            });
    }*/
    public synchronized void addToChat(Message msg){
        Task<HBox> othersMsg = new Task<HBox>(){

            @Override
            protected HBox call() throws Exception {
                String msgFull = msg.getName() + " : " + msg.getMsg();
                Label msgLabel = new Label();
                msgLabel.setText(msgFull);
                HBox x = new HBox();
                x.getChildren().addAll(msgLabel);
                return x;
            }
        };
        othersMsg.setOnSucceeded(event ->{
            chatPane.getItems().add(othersMsg.getValue());
        });
        Thread t = new Thread(othersMsg);
        t.setDaemon(true);
        t.start();
        }
}
