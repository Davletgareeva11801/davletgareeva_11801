package paint;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import server.ListOfWord;
import server.SecretWord;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuController {
    @FXML
    private BorderPane rootPane;
    @FXML
    private TextField usernameTextField;
    @FXML
    private Button play;
    @FXML
    private TextField hostnameTextField;
    @FXML
    private TextField portTextField;

    private Scene scene;
    private static MenuController instance;
    private static PaintController chatController;


    public void initialize(URL location, ResourceBundle resources){
        //updateName();
        //setNameOnClick();
    }
    public MenuController(){
        instance = this;
    }
    public static MenuController getInstance() {
        return instance;
    }

    public void handleButtonClick() throws IOException {
        String hostname = hostnameTextField.getText();
        int port = Integer.parseInt(portTextField.getText());
        String username = usernameTextField.getText();
        System.out.println(" host:  " + hostname + "  port:  " + port + "  name: "+username);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("paint.fxml"));
        Parent window = (BorderPane) fxmlLoader.load();
        
        chatController =  fxmlLoader.<PaintController>getController();
        Listener listener = new Listener(hostname, port, username, chatController);
        Thread x = new Thread(listener);
        x.start();
        this.scene = new Scene(window);

        //makeFadeOut(hostname, port,username);
    }
    public void showScene() throws IOException{
        Platform.runLater(() -> {
            Stage stage = (Stage) hostnameTextField.getScene().getWindow();
            stage.setWidth(600);
            stage.setHeight(600);
            stage.setOnCloseRequest((WindowEvent e) -> {
                Platform.exit();
                System.exit(0);
            });
            stage.setScene(this.scene);
            stage.setMinWidth(900);
            stage.setMaxHeight(900);
            stage.centerOnScreen();
            chatController.setUsernameLabel(usernameTextField.getText());
            chatController.setSecretWordLabel(SecretWord.showSecretWord());
        });
    }

    /*private void makeFadeOut(String hostname, int port , String username) {
        FadeTransition fadeTransition = new FadeTransition();
        fadeTransition.setDuration(Duration.millis(1000));
        fadeTransition.setNode(rootPane);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("between");
                loadNextScene(hostname, port, username) ;
            }
        });
        fadeTransition.play();
    }
    private void loadNextScene(String hostname, int port, String username){
        Parent secondView;
        try {
            //FXMLLoader fxmlLoader = (BorderPane)FXMLLoader();
            secondView = (BorderPane) FXMLLoader.load(getClass().getResource("paint.fxml"));
            //chatController =  .<ChatController>getController();

            Stage curStage = (Stage)rootPane.getScene().getWindow();
            curStage.setScene(new Scene(secondView));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

}
