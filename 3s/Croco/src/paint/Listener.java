package paint;

import client.Message;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Listener implements Runnable {
    private Socket socket;
    public String hostname;
    public int port;
    public static String username;
    public PaintController controller;
    private static DataOutputStream dout;
    private static ObjectOutputStream objectOutputStream;
    private static DataInputStream din;
    private static ObjectInputStream objectInputStream;
    private InputStream in;
    private OutputStream out;

    public Listener(String hostname, int port, String username, PaintController controller){
        //this.dout = new DataOutputStream(outputStream);
        //this.din = new DataInputStream(inputStream);
        this.hostname = hostname;
        this.port = port;
        Listener.username = username;
        this.controller = controller;
    }


    public void run(){
        try {
            socket = new Socket(hostname, port);
            MenuController.getInstance().showScene();
            out = socket.getOutputStream();
            //dout = new DataOutputStream(out);
            objectOutputStream = new ObjectOutputStream(out);
            in = socket.getInputStream();
            objectInputStream = new ObjectInputStream(in);
            //din = new DataInputStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //connect();
        while(socket.isConnected()){
            try {
                Message msg = null;
                try {
                    msg = (Message)objectInputStream.readObject();
                    System.out.println("получено сообщение: "+msg.getName() + ":" + msg.getMsg());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                if(msg!=null){
                    controller.addToChat(msg);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }

    public static void send(String msg) throws IOException {
        System.out.println("in send method: " + msg);
        Message newMessage = new Message();
        newMessage.setName(username);
        newMessage.setMsg(msg);
        System.out.println("in send method object: " + newMessage);
        objectOutputStream.writeObject(newMessage);
        objectOutputStream.flush();
        //dout.writeUTF(msg);
        //dout.flush();
    }


}
