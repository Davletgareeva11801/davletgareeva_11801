package paint;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Paint extends Application {
    public static String ip = null;
    //private Socket socket = null;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;
    public static String name;

    //final String HOST = "localhost";
    //final int PORT = 1234;


    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("/paint/menu.fxml"));
        primaryStage.setTitle("Crocodile");
        primaryStage.getIcons().add(new Image(new FileInputStream("C:\\Users\\alsud\\Documents\\Bitbucket\\3s\\Croco\\src\\paint\\crocodile.jpg")));
        Scene mainScene = new Scene(root,550,420);
        mainScene.setRoot(root);
        primaryStage.setScene(mainScene);

        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> Platform.exit());
    }

    public static void main(String[] args) {
        launch(args);
    }

}
