package client;

import java.io.Serializable;

public class Message implements Serializable {
    private String name;
    private String msg;

    public Message(){

    }

    public String getName() {
        return name;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setName(String name) {
        this.name = name;
    }
}