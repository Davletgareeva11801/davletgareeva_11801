package server;
import client.Message;
import paint.Paint;

import java.io.*;
import java.net.Socket;

public class Connection implements Runnable {
    private DataInputStream din;
    private DataOutputStream dout;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    private Socket client;
    private String name;
    private InputStream is;
    private OutputStream os;

    public Connection(Socket client){
        this.client = client;


    }

    @Override
    public void run() {
        try {
            is = client.getInputStream();
            objectInputStream = new ObjectInputStream(is);
            //din = new DataInputStream(is);
            os = client.getOutputStream();
            objectOutputStream = new ObjectOutputStream(os);
            //dout = new DataOutputStream(os);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (client.isConnected()){
            try {
                Message inputMessage = (Message) objectInputStream.readObject();

                //String inputmsg = din.readUTF();
                if(inputMessage != null){
                    write(inputMessage);
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }
    private void write(Message message){
        for (Connection conn : Server.clients){
            try {
                conn.getOut().writeObject(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public DataOutputStream getDout() {
        return dout;
    }
    public ObjectOutputStream getOut(){
        return objectOutputStream;
    }
}
