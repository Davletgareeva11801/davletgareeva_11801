package server;

public class SecretWord {
    private static SecretWord secretWord;

    private static String word = ListOfWord.randomWord();
    public static synchronized SecretWord getSecretWord(){
        if(secretWord == null){
            secretWord = new SecretWord();
        }
        return secretWord;
    }
    private SecretWord(){}
    public static void newSecretWord(){
        word = ListOfWord.randomWord();
    }
    public static String showSecretWord(){
        return word;
    }

    public static void main(String[] args) {
        System.out.println(SecretWord.getSecretWord().toString());
        System.out.println(SecretWord.showSecretWord());
        System.out.println(SecretWord.showSecretWord());
        System.out.println(SecretWord.showSecretWord());
        System.out.println(SecretWord.showSecretWord());

        SecretWord.newSecretWord();
        System.out.println(SecretWord.showSecretWord());
        System.out.println(SecretWord.showSecretWord());
        System.out.println(SecretWord.showSecretWord());
        System.out.println(SecretWord.showSecretWord());
        System.out.println(SecretWord.showSecretWord());

    }
}
