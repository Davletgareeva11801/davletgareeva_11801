package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Server {
    public static ArrayList<Connection> clients = null;
    //private static DataInputStream din;
    //private static DataOutputStream dout;
    public static void main(String[] args) throws IOException {
        int port = 1234;
        clients = new ArrayList<>();
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (true) {
                Socket client = serverSocket.accept();
                System.out.println("new client :" + client);
                Connection conn = new Connection(client);
                clients.add(conn);
                new Thread(conn).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
            /*String msgin = "";
            Socket client = serverSocket.accept();
            din = new DataInputStream(client.getInputStream());
            dout = new DataOutputStream(client.getOutputStream());
            Scanner scan = new Scanner(System.in);
            while(!msgin.equals("exit")){
                msgin = din.readUTF();
                System.out.println(msgin);

            }*/

    }

}

