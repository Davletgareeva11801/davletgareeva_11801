import org.junit.Assert;
import org.junit.Test;

import java.io.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Vector2DTest {

    @Test
    public void defaultVectorHasZeroX() {
        Vector2D v1 = new Vector2D();
        Assert.assertEquals(0, v1.getX(), 1e-9);
    }
    @Test
    public void checkCopyNBytes() throws IOException {
        InputStream is = mock(InputStream.class);
        when(is.read()).thenReturn(2).thenReturn(5);
        String filename ="src.txt";
        OutputStream os = new FileOutputStream(filename);
        Main.copy(is, os,2);
        os.close();
        FileInputStream fis = new FileInputStream(filename);
        Assert.assertTrue(fis.read() == 2 & fis.read() == 5 );

    }
    @Test
    public void checkSum5Bytes() throws IOException {
        InputStream is = mock(InputStream.class);

        when(is.read()).thenReturn(5);
        Assert.assertEquals(25, Main.sum(is));
    }


    @Test
    public void testSum() {
        Vector2D v1 = new Vector2D(1, 2);
        //Vector2D v2 = new Vector2D(5, 7);
        Vector2D v2 = mock(Vector2D.class);
        when(v2.getX()).thenReturn(5.0);
        when(v2.getY()).thenReturn(7.0);

        Assert.assertEquals(
                6, v1.add(v2).getX(), 1e-9
        );
    }
    public void testDownload() {

    }


}
