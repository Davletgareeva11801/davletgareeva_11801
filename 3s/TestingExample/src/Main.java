import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class Main {
    public static void copy(InputStream is, OutputStream os, int nBytes) throws IOException {
        for (int i = 0; i < nBytes; i++) {
            os.write(is.read());
        }
    }

    public static int sum(InputStream is) throws IOException {
        int sum = 0;
        for(int i = 0; i < 5; i++){
            sum += is.read();
        }
        return sum;
    }
    public static void download(URLConnection url) throws IOException {

        InputStream is = new BufferedInputStream(url.getURL().openStream());
        FileOutputStream fos = new FileOutputStream(new File("01.png"));
        int a = is.read();
        while(a != -1){
            fos.write(a);
            a = is.read();
        }
    }
    public static void main(String[] args) throws  IOException{
//        URL url = new URL("https://i.pinimg.com/originals/7e/66/c7/7e66c79bc2e80ac482872669992ddb28.jpg");
//        URLConnection urlcon = url.openConnection();
//        download(urlcon);

    }
}
