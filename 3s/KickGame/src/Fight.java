import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import sample.Main;

public class Fight {
    private Player player1;
    private Player player2;
    private Main mainapp;
    private IntegerProperty force1;
    private IntegerProperty force2;

    public void setMainapp(Main mainapp){
        this.mainapp = mainapp;
    }
    @FXML
    public void initialize(){
        player1 = new Player();
        player2 = new Player();
        player1hp.textProperty().bind(player1.getHPProperty().asString());
        player2hp.textProperty().bind(player2.getHPProperty().asString());
        force1 = new SimpleIntegerProperty();
        force2 = new SimpleIntegerProperty();
        force1.set(5);
        force2.set(5);
        tf1.textProperty().bind(force1.asString());
        Slider1.valueProperty().bindBidirectional(force1);
        tf2.textProperty().bind(force2.asString());
        Slider2.valueProperty().bindBidirectional(force2);

    }
    @FXML private Label player1hp;
    @FXML private Label player2hp;
    @FXML private TextField tf1;
    @FXML private TextField tf2;
    @FXML private Slider Slider1;
    @FXML private Slider Slider2;
    @FXML
    public void handleKick1(){
        player2.minusHP((int)Slider1.getValue());
    }
    public void handleKick2(){
        player1.minusHP((int)Slider2.getValue());
    }
}









