import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Player {
    private final StringProperty name;
    private final IntegerProperty hp;
    public Player() {
        name = new SimpleStringProperty("DefaultName");
        hp = new SimpleIntegerProperty(100);
    }
    public void minusHP(int d) {
        hp.set(hp.get() - d);
    }
    public IntegerProperty getHPProperty(){
        return hp;
    }
}