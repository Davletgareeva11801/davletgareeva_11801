/**
* @author Alsu Davletgareeva
* 11-801
* Task 16
*/

public class Task16 {
	
	public static int[] swap (int[] arrayFirst, int[] arraySecond){
		
		int[] arraySwap = new int[arrayFirst.length];
		for (int i = 0; i < arrayFirst.length; i++){
			arraySwap[i] = arraySecond[arrayFirst[i]];
		}
		return arraySwap;
	}
	
	public static void main (String [] args){
		
		int [] arrayFirst = {0, 2, 1};
		int [] arraySecond = {1, 2, 0};
		arrayFirst = swap(arrayFirst, arraySecond);
		for (int i = 0; i < arrayFirst.length; i++){
			System.out.print (arrayFirst[i]);
		}
	}
}
			