
public class Vector2D{
	
	final static double EPSILON = 0.00001;
	
	private double x, y;
	
	public Vector2D() {
	  /*this.x = 0;
		this.y = 0;*/
		this(0,0);
	}
	
	public String toString() {
		return "(" + x + "; " + y + ")";
	}
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	
	}
		
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public static Vector2D add(Vector2D first, Vector2D second) {
		return new Vector2D(first.getX() + second.getX(), first.getY() + second.getY());
	}
	
	public Vector2D add2(Vector2D second) {
		return new Vector2D(this.x + second.getX(), this.y + second.getY());
	}
	
	public static Vector2D sub(Vector2D first, Vector2D second) {
		return new Vector2D(first.getX() - second.getX(), first.getY() - second.getY());
	}
	
	public Vector2D sub2(Vector2D second) {
		return new Vector2D(this.x - second.getX(), this.y - second.getY());
	}
	
	public static Vector2D mult(Vector2D v, double number) {
		return new Vector2D(v.getX() * number, v.getY() * number);
	}
	
	public Vector2D mult2(double number) {
		return new Vector2D(this.x * number, this.y * number);
	}
	
	public double length() {
		return Math.sqrt(x * x + y * y);
	}
	
	public static double scalarProduct(Vector2D first, Vector2D second) {
		return first.getX() * second.getX() + first.getY() * second.getY();
	}
	
	public static double cos(Vector2D first, Vector2D second) {
		return scalarProduct(first, second)/((first.length() * second.length()));
	}
	
	public static boolean equals(Vector2D first, Vector2D second) {
		return ((Math.abs(first.getX() - second.getX())) < EPSILON) && ((Math.abs(first.getY() - second.getY())) < EPSILON);
	}
}
		
	

		