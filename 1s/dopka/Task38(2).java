/**
* @author Alsu Davletgareeva
* 11-801
* Task 38(2)
*/
import java.util.Random;
import java.util.regex.*;

public class Task38(2){
	public static void main(String[] args){
		Pattern p = Pattern.compile("(0-9)*(0|2|4|6|8){2}(0-9)*(0|2|4|6|8){2}(0-9)*");
		Random r = new Random();
		int count = 0;
		int countMatched = 0;
		while(countMatched != 10){
			int newInt = r.nextInt();
			Matcher m = p.matcher(Integer.toString(newInt));
			if(m.find()){
				System.out.println(m.group());
				countMatched++;
			}
			count++;
		}
		System.out.println(count);
	}
}