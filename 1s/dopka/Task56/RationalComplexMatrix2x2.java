/**
* @author Alsu Davletgareeva
* 11-801
* Task 56
*/
package ru.kfpu.itis.group801.Davletgareeva.basicClasses;

public class RationalComplexMatrix2x2 {
	private RationalComplexNumber[][] matrix;
	
	public RationalComplexMatrix2x2(){
		matrix[0][0] = new RationalComplexNumber();
		matrix[0][1] = new RationalComplexNumber();
		matrix[1][0] = new RationalComplexNumber();
		matrix[1][1] = new RationalComplexNumber();
	}
	
	public RationalComplexMatrix2x2(RationalComplexNumber x){
		matrix[0][0] = x;
		matrix[0][1] = x;
		matrix[1][0] = x;
		matrix[1][1] = x;
	}
	
	public RationalComplexMatrix2x2(RationalComplexNumber x1, RationalComplexNumber x2,
									RationalComplexNumber x3, RationalComplexNumber x4){
		matrix[0][0] = x1;
		matrix[0][1] = x2;
		matrix[1][0] = x3;
		matrix[1][1] = x4;
	}
	
	public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 secondRCM){
		return new RationalComplexMatrix2x2(this.matrix[0][0].add(secondRCM[0][0]),
											this.matrix[0][1].add(secondRCM[0][1]),
											this.matrix[1][0].add(secondRCM[1][0]),
											this.matrix[1][1].add(secondRCM[1][1]));
	}
	
	public static RationalComplexNumber multAndSum(RationalComplexNumber[] a, RationalComplexNumber[] b){
		RationalComplexNumber result = new RationalComplexNumber();
		for(int i = 0; i < a.length; i++){
			result = result.add(a[i].mult(b[i]));
		}
		return result;
	}
	
	public static RationalComplexNumber[] getColumn(RationalComplexNumber[][] a, int i){
		RationalComplexNumber[] result = new RationalComplexNumber[a.length];
		for(int j = 0; j < a.length; j++){
			result[j] = a[j][i];
		}
		return result;
	}
	
	public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 secondRCM){
		return new RationalComplexMatrix2x2(multAndSum(this.matrix[0], getColumn(secondRCM, 0)),
											multAndSum(this.matrix[0], getColumn(secondRCM, 1)),
											multAndSum(this.matrix[1], getColumn(secondRCM, 0)),
											multAndSum(this.matrix[1], getColumn(secondRCM, 1)));
	}
	
	public RationalComplexNumber det(){
		return new RationalComplexNumber(matrix[0][0].mult(matrix[1][1]).getRe().sub(matrix[0][1].mult(matrix[1][0]).getRe()),
											matrix[0][0].mult(matrix[1][1]).getIm().sub(matrix[0][1].mult(matrix[1][0]).getIm()));
	}
	
	public RationalComplexVector2D multVector(RationalComplexVector2D rcv){
		return new RationalComplexVector2D(matrix[0][0].mult(rcv.getX()).add(matrix[0][1].mult(rcv.getY())), 
											matrix[1][0].mult(rcv.getX()).add(matrix[1][1].mult(rcv.getY())));
	}
	
}