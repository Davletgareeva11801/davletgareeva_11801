public class Sedan extends Car {
	public String drive;
	
	public Sedan(int maxSpeed, String model){
		super(maxSpeed, model);
		this.drive = null;
	}
	
	public Sedan(int maxSpeed, String model, String drive){
		this(maxSpeed, model);
		this.drive = drive;
	}
	
	public String typeOfDrive(){
		return drive;
	}
	
	public void showInformation(){
		super.showInformation();
		System.out.println("drive: " + drive);
	}
	
	public void gas(){
		System.out.println("The sedan starts moving");
	}
	
	public void buy(){
		System.out.println("You have bought this sedan");
	}
}