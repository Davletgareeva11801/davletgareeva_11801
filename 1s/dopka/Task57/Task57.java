/**
* @author Alsu Davletgareeva
* 11-801
* Task 57
*/
public class Task57 {
	
	public static void main(String[] args){
		Car[] car = new Car[2];
		car[0] = new Sedan(250, "Reno", "front-wheel");
		car[1] = new Jeep(370, "BMV", "black");
		car[0].showInformation();
		car[0].buy();
		car[1].showInformation();
		car[1].buy();
	}
	
}