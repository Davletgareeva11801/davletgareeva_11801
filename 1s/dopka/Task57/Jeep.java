public class Jeep extends Car {
	private String colour;
	
	public Jeep(int maxSpeed, String model){
		super(maxSpeed, model);
		this.colour = null;
	}
	
	public Jeep(int maxSpeed, String model, String colour){
		this(maxSpeed, model);
		this.colour = colour;
	}
	
	public void readColour(){
		System.out.println(colour);
	}
	
	public void swowInformation(){
		super.showInformation();
		System.out.println("colour: " + colour);
	}
	
	public void gas(){
		System.out.println("The jeep revs");
	}
	
	public void buy(){
		System.out.println("You have bought this car");
	}
	
}
		