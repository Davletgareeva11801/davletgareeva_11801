public abstract class Car{
	
	protected int maxSpeed;
	protected String model;
	
	public Car(int maxSpeed, String model){
		this.maxSpeed = maxSpeed;
		this.model = model;
	}
	
	public Car(){
		this(0, null);
	}
	
	public void showInformation(){
		System.out.println("model: " + model +"; maxSpeed: " + maxSpeed);
	}
	
	abstract public void buy();
	
	abstract public void gas();
	
}