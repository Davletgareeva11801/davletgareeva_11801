/**
* @author Alsu Davletgareeva
* 11-801
* Task 35
*/

import java.util.regex.*;
public class Task35{
	public static void main(String[] args){
		String[] a = {"0101", "0", "110", "11"};
		Pattern p = Pattern.compile("(0+)|(1+)|((01)+0?)|((10)+1?)");
		for(int i = 0; i < a.length; i++){
			Matcher m = p.matcher(a[i]);
			if(m.matches()){
				System.out.println(i);
			}
		}
	}
}

