/**
* @author Alsu Davletgareeva
* 11-801
* Task 53
*/
package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
public class ComplexMatrix2x2 {
	private ComplexNumber[][] matrix;
	
	public ComplexMatrix2x2(){
		matrix[0][0] = new ComplexNumber();
		matrix[0][1] = new ComplexNumber();
		matrix[1][0] = new ComplexNumber();
		matrix[1][1] = new ComplexNumber();
	}
	
	public ComplexMatrix2x2(ComplexNumber x){
		matrix[0][0] = x;
		matrix[0][1] = x;
		matrix[1][0] = x;
		matrix[1][1] = x;
	}
	
	public ComplexMatrix2x2(ComplexNumber x1, ComplexNumber x2, ComplexNumber x3, ComplexNumber x4){
		matrix[0][0] = x1;
		matrix[0][1] = x2;
		matrix[1][0] = x3;
		matrix[1][1] = x4;
	}
	
	public ComplexMatrix2x2 add(ComplexMatrix2x2 secondCM){
		return new ComplexMatrix2x2(this.matrix[0][0].add(secondCM[0][0]),
									this.matrix[0][1].add(secondCM[0][1]),
									this.matrix[1][0].add(secondCM[1][0]),
									this.matrix[1][1].add(secondCM[1][1]));
	}
	
	public static ComplexNumber multAndSum(ComplexNumber[] x, ComplexNumber[] y){
		ComplexNumber result = new ComplexNumber();
		for(int i = 0; i < x.length; i++){
			result = result.add(x[i].mult(y[i]));
		}
		return result;
	}
	
	public static ComplexNumber[] getColumn(ComplexNumber[][] x, int i){
		ComplexNumber[] result = new ComplexNumber[x.length];
		for(int j = 0; j < x.length; j++){
			result[j] = x[j][i];
		}
		return result;
	}
	
	public ComplexMatrix2x2 mult(ComplexMatrix2x2 secondCM){
		return new ComplexMatrix2x2(multAndSum(this.matrix[0], getColumn(secondCM, 0)),
									multAndSum(this.matrix[0], getColumn(secondCM, 1)),
									multAndSum(this.matrix[1], getColumn(secondCM, 0)),
									multAndSum(this.matrix[1], getColumn(secondCM, 1)));
	}
	
	public ComplexNumber det(){
		return new ComplexNumber(matrix[0][0].mult(matrix[1][1]).getRe() - matrix[0][1].mult(matrix[1][0]).getRe(),
								matrix[0][0].mult(matrix[1][1]).getIm() - matrix[0][1].mult(matrix[1][0]).getIm());
	}
	
	public ComplexVector2D multVector(ComplexVector2D cv){
		return new ComplexVector2D(matrix[0][0].mult(cv.getX()).add(matrix[0][1].mult(cv.getY())), 
								matrix[1][0].mult(cv.getX()).add(matrix[1][1].mult(cv.getY())));
	}
	
}
		