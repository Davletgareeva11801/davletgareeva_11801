/**
* @author Alsu Davletgareeva
* 11-801
* Task 31
*/
import java.util.*;

public class Task31{
	
	public static String getString(String str, int i, int length){
		String res = new String();
		for(int k = i; k < i + length; k++){
			res += str.charAt(k);
		}
		return res;
	}
	
	public static ArrayList<Integer> indexesOfFinding(String str, String s){
		ArrayList<Integer> res = new ArrayList<>();
		for(int i = 0; i <= str.length() - s.length(); i++){
			if(getString(str, i, s.length()).equals(s)){
				res.add(i);
			}
		}
		return res;
	}
	
	public static String deleteString(String str, int x, int length){
		String res = new String();
		for(int i = 0; i < x; i++){
			res += str.charAt(i);
		}
		for(int i = x; i < str.length() - length; i++){
			res += str.charAt(i + length);
		}
		return res;
	}
	
	public static String addString(String str, String s, int i){
		String res = new String();
		for(int k = 0; k < i; k++){
			res += str.charAt(k);
		}
		res += s;
		for(int k = i; k < str.length(); k++){
			res += str.charAt(k);
		}
		return res;
	}
	
	public static String change(String str, String s, int length, int i){
		String res = new String();
		res = deleteString(str, i, length);
		res = addString(res, s, i);
		return res;
	}
	
	public static void main(String[] args){
		String str = "false true true";
		int count = 0;
		while(count < indexesOfFinding(str, "true").size()){
			if((count + 1) % 2 == 0){
				str = change(str, "false", 4, indexesOfFinding(str, "true").get(count));
			}
			count++;
		}
		System.out.println(str);
	}
	
}