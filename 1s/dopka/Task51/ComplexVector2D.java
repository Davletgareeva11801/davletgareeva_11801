/**
* @author Alsu Davletgareeva
* 11-801
* Task 51
*/
package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
public class  ComplexVector2D {
	private ComplexNumber x;
	private ComplexNumber y;
	
	public ComplexNumber getX(){
		return x;
	}
	
	public ComplexNumber getY(){
		return y;
	}
	
	public ComplexVector2D(){
		x = new ComplexNumber();
		y = new ComplexNumber();
	}
	
	public ComplexVector2D(ComplexNumber x, ComplexNumber y){
		this.x = x;
		this.y = y;
	}
	
	public ComplexVector2D add(ComplexVector2D second){
		return new ComplexVector2D(this.x.add(second.x), this.y.add(second.y));
	}
	
	public String toString(){
		return "(" + x.toString() + "; " + y.toString() + ")";
	}
	
	public ComplexNumber scalarProduct(ComplexVector2D second){
		return new ComplexNumber(this.x.mult(second.x).getRe() + this.y.mult(second.y).getRe(),
		this.x.mult(second.x).getIm() + this.y.mult(second.y).getIm());
	}
	
	public boolean equals(ComplexVector2D second){
		return this.x.equals(second.x) && this.y.equals(second.y);
	}
	
}