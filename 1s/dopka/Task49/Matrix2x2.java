/**
* @author Alsu Davletgareeva
* 11-801
* Task 49
*/
package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
class Matrix2x2 {
	
	private double[][] matrix;
	
	public Matrix2x2() {
		
		for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 2; j++) {
                this.matrix[i][j] = 0;
            }
        }
    }
	
	public Matrix2x2(double number) {
		
		for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 2; j++) {
                this.matrix[i][j] = number;
            }
        }
    }
	
	public Matrix2x2(double [][] inputMatrix) {
		
		for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 2; j++) {
                this.matrix[i][j] = inputMatrix[i][j];
            }
        }
    }
	
	public Matrix2x2(double a, double b, double c, double d) {
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}
	
	public Matrix2x2 add(Matrix2x2 second){
		return new Matrix2x2(this.matrix[0][0] + second[0][0],
							this.matrix[0][1] + second[0][1],
							this.matrix[1][0] + second[1][0], 
							this.matrix[1][1] + second[1][1]);
	}
	
	public void add2(Matrix2x2 second){
		this.matrix[0][0] += second[0][0];
		this.matrix[0][1] += second[0][1];
		this.matrix[1][0] += second[1][0];
		this.matrix[1][1] += second[1][1];
	}
	
	public Matrix2x2 sub(Matrix2x2 m){
		return new Matrix2x2(this.matrix[0][0] - second[0][0],
							this.matrix[0][1] - second[0][1],
							this.matrix[1][0] - second[1][0], 
							this.matrix[1][1] - second[1][1]);
	}
	
	public void sub2(Matrix2x2 m){
		this.matrix[0][0] -= second[0][0];
		this.matrix[0][1] -= second[0][1];
		this.matrix[1][0] -= second[1][0];
		this.matrix[1][1] -= second[1][1];
	}
	
	public Matrix2x2 multNumber(double number){
		return new Matrix2x2(this.matrix[0][0] * number,
							this.matrix[0][1] * number,
							this.matrix[1][0] * number,
							this.matrix[1][1] * number);
	}
	
	public void multNumber2(double number){
		this.matrix[0][0] *= number;
		this.matrix[0][1] *= number;
		this.matrix[1][0] *= number;
		this.matrix[1][1] *= number;
	}
	
	public static double multAndSum(double[] x, double[] y){
		double result = 0;
		for(int i = 0; i < x.length; i++) {
			result += x[i] * y[i];
		}
		return result;
	}
	
	public static double[] getColumn(double[][] x, int i){
		double[] result = new double[x.length];
		for(int j = 0; j < x.length; j++){
			result[j] = x[j][i];
		}
		return result;
	}
	
	public Matrix2x2 mult(Matrix2x2 second) {
		return new Matrix2x2(multAndSum(this.matrix[0], getColumn(second, 0)),
							multAndSum(this.matrix[0], getColumn(second, 1)),
							multAndSum(this.matrix[1], getColumn(second, 0)),
							multAndSum(this.matrix[1], getColumn(second, 1)));
	}
		
	public void mult2(Matrix2x2 second) {
		this.matrix[0][0] = multAndSum(this.matrix[0], getColumn(second, 0));
		this.matrix[0][0] = multAndSum(this.matrix[0], getColumn(second, 1));
		this.matrix[1][0] = multAndSum(this.matrix[1], getColumn(second, 0));
		this.matrix[1][0] = multAndSum(this.matrix[1], getColumn(second, 1));
	}
	
	public static double det() {
		return this.matrix[0][0] * this.matrix[1][1] - this.matrix[0][1] * this.matrix[1][0];
	}
	
	public void transpon(){
		double temp = matrix[0][1];
		matrix[0][1] = matrix[1][0];
		matrix[1][0] = temp;
	}
	
	public static Matrix2x2 inverseMatrix() throws Exception {
		if (this.matrix.det() == 0) {
			throw new Exception ("this matrix cannot be inversed");
		} else {
			return(this.matrix.transpon().multNumber2(this.matrix.det());
		}
	}
	
	public static Matrix2x2 equivalentDiagonal(){
		Matrix2x2 result = new Matrix2x2(this.matrix);
		if(result[0][0] == 0){
			for(int i = 0; i < 2; i++){
				double t = res.matrix[0][i];
				res.matrix[0][0] = res.matrix[1][i];
				res.matrix[1][i] = t;
			}
		}
		if(result[1][0] != 0){
			result[1][1] -= result[0][1] * result[1][0] / result[0][0];
			result[1][0] = 0;
		}
		if(res.matrix[0][1] != 0){
			res.matrix[0][0] -= result[1][0] * result[0][1] / result.matrix[1][1];
			result[0][1] = 0;
		}
		return result;
	}
		
	public static Vector2D multVector(Vector2D vector) {
		return new Vector2D(this.matrix[0][0] * vector.getX() + this.matrix[0][1] * vector.getY(), 
							this.matrix[1][0] * vector.getX() + this.matrix[1][1] * vector.getY());
	}
}
		
		
		
		
	
	