/**
* @author Alsu Davletgareeva
* 11-801
* Task 58
*/
public interface Number {
    public Number add(Number n);
    public Number sub(Number n);
    public int compareTo(Number n);
    public String getNumber();
}
