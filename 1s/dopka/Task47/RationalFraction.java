package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
class RationalFraction {
	
	private int num, den;
	
	public RationalFraction() {
		this(0,1);
	}
	
	public RationalFraction(int num, int den) {
		this.num = num;
		this.den = den;
	}
	
	public String toString() {
		return num + "/" + den;
	}
	
	public int getNum() {
		return num;
	}
	
	public int getDen() {
		return den;
	}
	
	public void reduce() {
		int n = Math.abs(this.num);
		int m = Math.abs(this.den);
		while (n != m) {
			if (n > m) {
				n = n - m;
			} else {
				m = m - n;
			}
		}
		//System.out.println(new RationalFraction((first.getNum() / n),(first.getDen() / n)));
		this.num = this.getNum() / n;
		this.den = this.getDen() / n;
	}
	
	public static RationalFraction add(RationalFraction first, RationalFraction second) {
		return new RationalFraction((first.getNum()*second.getDen() + second.getNum()*first.getDen()),
																	first.getDen()*second.getDen());
	}
	
	public void add2(RationalFraction second) {
		this.num = this.getNum()*second.getDen() + second.getNum()*this.getDen();
		this.den = this.getDen()*second.getDen();
	}
	
	public static RationalFraction sub(RationalFraction first, RationalFraction second) {
		return new RationalFraction((first.getNum() * second.getDen() - second.getNum() * first.getDen()),
																	first.getDen() * second.getDen());
	}
	
	public void sub2(RationalFraction second) {
		this.num = this.getNum() * second.getDen() - second.getNum() * this.getDen();
		this.den = this.getDen() * second.getDen();
	}
	
	public static RationalFraction mult(RationalFraction first, RationalFraction second) {
		return new RationalFraction(first.getNum() * second.getNum(), first.getDen() * second.getDen());
	}
	
	public void mult2(RationalFraction second) {
		this.num = this.getNum() * second.getDen();
		this.den = this.getDen() * second.getDen();
	}
	
	public static RationalFraction div(RationalFraction first, RationalFraction second) {
		return new RationalFraction(first.getNum() * second.getDen(), first.getDen() * second.getNum());
	}
	
	public void div2(RationalFraction second) {
		this.num = this.getNum() * second.getDen();
		this.den = this.getDen() * second.getNum();
	}
	
	public double value() {
		return ((double)this.getNum() / this.getDen());
	}
	
	public static boolean equals(RationalFraction first, RationalFraction second) {
		if (((double)first.getNum() / second.getNum()) == ((double)first.getDen() / second.getDen())) {
		//if ((first.getNum() == second.getNum()) && (first.getDen() == second.getDen())) {
			return true;
		} else {
			return false;
		}
	}
	
	public int numberPart() {
		return this.getNum() / this.getDen();
	}
		
	
	 
		
		
		
		
		
		
		
}	
		
