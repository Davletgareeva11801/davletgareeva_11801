/**
* @author Alsu Davletgareeva
* 11-801
* Task 55
*/
package ru.kfpu.itis.group801.Davletgareeva.basicClasses;

public class RationalComplexVector2D {
		
	private RationalComplexNumber x;
	private RationalComplexNumber y;
	
	public RationalComplexNumber getX(){
		return x;
	}
	
	public RationalComplexNumber getY(){
		return y;
	}
	
	public RationalComplexVector2D(){
		x = new RationalComplexNumber();
		y = new RationalComplexNumber();
	}
	
	public RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y){
		this.x = x;
		this.y = y;
	}
	
	public RationalComplexVector2D add(RationalComplexVector2D secondRCV){
		return new RationalComplexVector2D(this.x.add(secondRCV.x), this.y.add(secondRCV.y));
	}
	
	public String toString(){
		return "(" + x.toString() + "; " + y.toString() + ")";
	}
	
	public RationalComplexNumber scalarProduct(RationalComplexVector2D rcv){
		return this.x.mult(rcv.x).add(this.y.mult(rcv.y));
	}
	
}