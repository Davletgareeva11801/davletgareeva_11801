package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
class ComplexNumber {
	
	private double re;
	private double im;
	
	public ComplexNumber() {
		this(0, 0);
	}
	
	public ComplexNumber(double re, double im) {
		this.re = re;
		this.im = im;
	}
	
	public double getRe() {
		return re;
	}
	
	public double getIm() {
		return im;
	}
	
	public String toString() {
		if (im < 0) {
			return re + " - " + Math.abs(im) + "*i";
		} else {
			return re + " + " + im + "i";
		}
		
	}
	
	public ComplexNumber add(ComplexNumber second) {
		return new ComplexNumber(this.getRe() + second.getRe(), this.getIm() + second.getIm());
	}
	
	public void add2(ComplexNumber second) {
		this.re = this.getRe() + second.getRe();
		this.im = this.getIm() + second.getIm();
	}
	
	public ComplexNumber sub(ComplexNumber second) {
		return new ComplexNumber(this.getRe() - second.getRe(), this.getIm() - second.getIm());
	}
	
	public void sub2(ComplexNumber second) {
		this.re = this.getRe() - second.getRe();
		this.im = this.getIm() - second.getIm();
	}
	
	public ComplexNumber multNumber(double number) {
		return new ComplexNumber(this.getRe() * number, this.getIm() * number);
	}
	
	public void multNumber2(double number) {
		this.re = this.getRe() * number;
		this.im = this.getIm() * number;
	}
	
	public ComplexNumber mult(ComplexNumber second) {
		return new ComplexNumber((this.getRe() * second.getRe() - this.getIm() * second.getIm()),
									(this.getIm() * second.getRe() + this.getRe() * second.getIm()));
	}
	
	public void mult2(ComplexNumber second) {
		double real = this.getRe();
		this.re = this.getRe() * second.getRe() - this.getIm() * second.getIm();
		this.im = this.getIm() * second.getRe() + real * second.getIm();
	}
	
	public ComplexNumber div(ComplexNumber second) {
		return new ComplexNumber((this.getRe() * second.getRe() + this.getIm() * second.getIm())/(second.getRe() * second.getRe() + second.getIm() * second.getIm()) ,
								(this.getIm() * second.getRe() - this.getRe() * second.getIm())/(second.getRe() * second.getRe() + second.getIm() * second.getIm()));
	}								
	
	public void div2(ComplexNumber second) {
		double real = this.getRe();
		this.re = (this.getRe() * second.getRe() + this.getIm() * second.getIm())/(second.getRe() * second.getRe() + second.getIm() * second.getIm());
		this.im = (this.getIm() * second.getRe() - real * second.getIm()) / (second.getRe() * second.getRe() + second.getIm() * second.getIm());
	}
									
	public double length() {
		return Math.sqrt(this.re * this.re + this.im * this.im);
	}
	
	public double arg() {
		return Math.atan(this.im / this.re);
	}
	
	public ComplexNumber pow(double n) {
		return new ComplexNumber(Math.pow(this.length(),n) * Math.cos(n * this.arg()), Math.sin(n * this.arg()));
	}
	
	public boolean equals(ComplexNumber second) {
		if ((this.re == second.getRe()) && (this.im == second.getIm())) {
			return true;
		} else {
			return false;
		}
	}
	
}
			
		
		