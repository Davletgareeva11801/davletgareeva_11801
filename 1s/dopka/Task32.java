/**
* @author Alsu Davletgareeva
* 11-801
* Task 32
*/
import java.util.*;

public class Task32{
	
public static String getString(String str, int i, int length){
		String res = new String();
		for(int k = i; k < i + length; k++){
			res += str.charAt(k);
		}
		return res;
	}
	
	public static boolean findString(String str, String s){
		for(int i = 0; i <= str.length() - s.length(); i++){
			if(getString(str, i, s.length()).equals(s)){
				return true;
			}
		}
		return false;
	}
	
	public static int indexOfFinding(String str, String s){
		for(int i = 0; i <= str.length() - s.length(); i++){
			if(getString(str, i, s.length()).equals(s)){
				return i;
			}
		}
		return -1;
	}
	
	public static String deleteString(String str, int x, int length){
		String res = new String();
		for(int i = 0; i < x; i++){
			res += str.charAt(i);
		}
		for(int i = x; i < str.length() - length; i++){
			res += str.charAt(i + length);
		}
		return res;
	}
	
	public static String addString(String str, String s, int i){
		String res = new String();
		for(int k = 0; k < i; k++){
			res += str.charAt(k);
		}
		res += s;
		for(int k = i; k < str.length(); k++){
			res += str.charAt(k);
		}
		return res;
	}
	
	public static String change(String str, String s, int length, int i){
		String res = new String();
		res = deleteString(str, i, length);
		res = addString(res, s, i);
		return res;
	}
	
	public static void main(String[] args){
		String str = "dad mom dad";
		while(findString(str, "mom")){
			str = change(str, "dad", 3, indexOfFinding(str, "mom"));
		}
		System.out.println(str);
	}
	
}