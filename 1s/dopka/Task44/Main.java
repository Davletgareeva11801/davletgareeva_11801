public class Main {
	
	public static void main(String [] args) {
		Player player1 = new Player(Tom, 20);
		Player player2 = new Player(Jerry, 25);
		Game game = new Game(player1, player2);
		game.start();
	}
}
