package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
public class Player {
	
	String name;
	int hp;
	
	
	public Player(String name, int hp) {
		this.name = name;
		this.hp = hp;
	}
	
	public String getName() {
		return name;
	}
	
	public int getHp() {
		return hp;
	}
	
	public IsHitFor(int power) {
		this.hp -= power;
	}
}