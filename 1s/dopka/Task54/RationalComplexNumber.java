/**
* @author Alsu Davletgareeva
* 11-801
* Task 54
*/
package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
public class RationalComplexNumber {
	private RationalFraction re;
	private RationalFraction im;
	
	public RationalFraction getRe(){
		return re;
	}
	
	public RationalFraction getIm(){
		return im;
	}
	
	public RationalComplexNumber(){
		re = new RationalFraction();
		im = new RationalFraction();
	}
	
	public RationalComplexNumber(RationalFraction re, RationalFraction im){
		this.re = re;
		this.im = im;
	}
	
	public RationalComplexNumber add(RationalComplexNumber secondRCN){
		return new RationalComplexNumber(this.re.add(secondRCN.re), this.im.add(secondRCN.im));
	}
	
	public RationalComplexNumber sub(RationalComplexNumber secondRCN){
		return new RationalComplexNumber(this.re.sub(secondRCN.re), this.im.sub(secondRCN.im));
	}
	
	public RationalComplexNumber mult(RationalComplexNumber secondRCN){
		return new RationalComplexNumber(this.re.mult(secondRCN.re).sub(this.im.mult(secondRCN.im)), 
										this.re.mult(secondRCN.im).add(this.im.mult(secondRCN.re)));
	}
	
	public String toString(){
		if(im.getNum * im.getDen() > 0){
			return re.toString() + " + " + im.toString() + " * i";
		}
		else if(im.getNum() * im.getDen) < 0){
			return re.toString() + " - " + (new RationalFraction().sub(im)).toString() + " * i";
		}
		else{
			return re.toString();
		}
	}
	
}