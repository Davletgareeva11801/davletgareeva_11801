/**
* @author Alsu Davletgareeva
* 11-801
* Task 50
*/
package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
public class RationalVector2D {
	private RationalFraction x;
	private RationalFraction y;
	
	public RationalFraction getX(){
		return x;
	}
	
	public RationalFraction getY(){
		return y;
	}
	
	public RationalVector2D(){
		x = new RationalFraction();
		y = new RationalFraction();
	}
	
	public RationalVector2D(RationalFraction x, RationalFraction y){
		this.x = x;
		this.y = y;
	}
	
	public RationalVector2D add(RationalVector2D second){
		return new RationalVector2D(this.x.add(second.getX), this.y.add(second.getY));
	}
	
	public String toString(){
		return "(" + x.toString() + "; " + y.toString() + ")";
	}
	
	public double length(){
		return Math.sqrt((x.mult(x).add(y.mult(y))).value());
	}
	
	public RationalFraction scalarProduct(RationalVector2D second){
		return this.x.mult(second.x).add(this.y.mult(second.y));
	}
	
	public boolean equals(RationalVector2D second){
		return this.x.equals(second.x) && this.y.equals(second.y);
	}
	
}
	
	