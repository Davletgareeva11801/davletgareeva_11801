/**
* @author Alsu Davletgareeva
* 11-801
* Task 52
*/
package ru.kfpu.itis.group801.Davletgareeva.basicClasses;
public class RationalMatrix2x2 {
	
	private RationalFraction[][] matrix;
	
	public RationalMatrix2x2(){
		matrix[0][0] = new RationalFraction();
		matrix[0][1] = new RationalFraction();
		matrix[1][0] = new RationalFraction();
		matrix[1][1] = new RationalFraction();
	}
	
	public RationalMatrix2x2(RationalFraction rf){
		matrix[0][0] = rf;
		matrix[0][1] = rf;
		matrix[1][0] = rf;
		matrix[1][1] = rf;
	}
	
	public RationalMatrix2x2(RationalFraction rf1, RationalFraction rf2, RationalFraction rf3, RationalFraction rf4){
		matrix[0][0] = rf1;
		matrix[0][1] = rf2;
		matrix[1][0] = rf3;
		matrix[1][1] = rf4;
	}
	
	public RationalMatrix2x2 add(RationalMatrix2x2 secondRM){
		return new RationalMatrix2x2(this.matrix[0][0].add(secondRM[0][0]), this.matrix[0][1].add(secondRM[0][1]),
									this.matrix[1][0].add(secondRM[1][0]), this.matrix[1][1].add(secondRM[1][1]));
	}
	
	public static RationalFraction multAndSum(RationalFraction[] x, RationalFraction[] y){
		RationalFraction result = new RationalFraction();
		for(int i = 0; i < x.length; i++){
			result = res.add(x[i].mult(y[i]));
		}
		return result;
	}
	
	public static RationalFraction[] getColumn(RationalFraction[][] x, int i){
		RationalFraction[] result = new RationalFraction[x.length];
		for(int j = 0; j < x.length; j++){
			result[j] = x[j][i];
		}
		return result;
	}
	
	public RationalMatrix2x2 mult(RationalMatrix2x2 secondRM){
		return new RationalMatrix2x2(multAndSum(this.matrix[0], getColumn(secondRM, 0)),
									multAndSum(this.matrix[0], getColumn(secondRM, 1)),
									multAndSum(this.matrix[1], getColumn(secondRM, 0)),
									multAndSum(this.matrix[1], getColumn(secondRM, 1)));
	}
	
	public RationalFraction det(){
		return matrix[0][0].mult(matrix[1][1]).sub(matrix[0][1].mult(matrix[1][0]));
	}
	
	public RationalVector2D multVector(RationalVector2D secondRM){
		return new RationalVector2D(matrix[0][0].mult(secondRM.getX()).add(matrix[0][1].mult(secondRM.getY())), 
									matrix[1][0].mult(secondRM.getX()).add(matrix[1][1].mult(secondRM.getY())));
	}
	
}