import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task2 {

    public static void countSort(ArrayList<Integer> array) {
        int l = (int) Collections.min(array);
        int r = (int) Collections.max(array);
        int[] c = new int[r-l+1];

        for (int x : array) {
            c[x-l]++;
        }
        int n = 0;

        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < c[i]; j++) {
                array.set(n++, i+l);
            }
        }
    }

    public static void  main(String[] args) {
        ArrayList<Integer> temperature = new ArrayList<Integer>();
        temperature.add(-3);
        temperature.add(20);
        temperature.add(-70);
        countSort(temperature);
        for(int x : temperature) {
            System.out.println(x);
        }

    }
}
