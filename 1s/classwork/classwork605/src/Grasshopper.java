public class Grasshopper {

    public static int numberOfTrajectories(int n) {
        int[] K = new int[n+1];
        K[0] = 0;
        K[1] = 1;
        K[2] = 1;
        for(int i = 3; i <= n; i++){
            K[i] = K[i - 1] + K[i - 2] + K[i - 3];
        }
        return K[n];
    }

    public static void main(String[] args) {
        System.out.println(numberOfTrajectories(5));
    }
}
