import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static java.util.Arrays.sort;

public class Task1 {

    public static int findNumberOfCar(int maxSum, int[]cars) {
        int i = 0;
        int count = 0;
        sort(cars);
        while((maxSum > 0)&&(i < cars.length)) {
            maxSum -= cars[i];
            i++;
            count++;
        }
       if (maxSum < 0){
            count --;
       }
        return count;
    }

    public static void main(String[] args) {
        int[] cars = {15, 5, 10, 11, 1};
        int Sum = 30;
        System.out.println(findNumberOfCar(Sum, cars));

    }
}
