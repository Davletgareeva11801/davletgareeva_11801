public class Elem<T> {

    private T value;
    private Elem<T> next;

    public Elem() {
        this.value = null;
        this.next = null;
    }

    public Elem(T value, Elem<T> next) {
        this.value = value;
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public Elem<T> getNext() {
        return next;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setNext(Elem<T> next) {
        this.next = next;
    }
}
