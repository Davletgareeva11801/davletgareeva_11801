public class LinkedStack<T> implements IStack<T> {

    private Elem head;

        public boolean isEmpty() {
            if (head == null) {
                return true;
            }else {
                return false;
            }

        }

        public void push(T elem) {
            Elem p = new Elem();
            p.setValue(elem);
            p.setNext(head.getNext());
            head.setNext(p);
        }

        public T pop() {
            T temp = (T) head.getValue();
            head = head.getNext();
            return temp;
        }

        public T peek() {
            return (T) head.getValue();
        }
}

