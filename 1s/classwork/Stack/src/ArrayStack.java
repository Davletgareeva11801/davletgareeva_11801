public class ArrayStack <T> implements IStack<T> {

    private Object[] array;
    private int top;

    public ArrayStack(int s) {
        array = (T[]) new Object[s];
        top = -1;
    }

    public void push(T element) {
        array[++top] = element;
    }

    public T pop() {
        if (isEmpty()) {
            System.out.println("Stack is Empty");
            return null;
        } else {
            return (T) array[top--];
        }
    }

    public T peek() {
        if (isEmpty()) {
            System.out.println("Stack is Empty");
            return null;
        }
        return (T) array[top];

    }

    public boolean isEmpty() {
        return (top == -1);
    }



    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack(5);
        stack.push(5);
        stack.push(2);
        stack.push(1);

    }
}

