public class Array3 {
	
	public static boolean symmetry (int [] arr){
		int n = arr.length;
		for (int i = 0; i < (n / 2); i++){
			if (arr[i] != arr[n - i - 1]){
				 return false;
			 }
		}
		return true;
	}
	
	public static void main (String [] args){
		int [] arr = {1, 2, 3, 3, 2, 1};
		System.out.println(symmetry(arr));
	}
}