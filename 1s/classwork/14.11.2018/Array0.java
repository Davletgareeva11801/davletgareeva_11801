public class Array0{
	
	public static int findMax (int [] arr ){
		int max = arr[0];
		for (int x: arr){
			if (x > max){
				max = x;
			}
		}
		return max;
	}

	public static void main (String [] args) {
		int [] arr = {1, 2, 0};
		
		System.out.println(findMax(arr));
	}
}
		