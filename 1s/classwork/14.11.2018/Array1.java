public class Array1{
	
	public static int findSum (int [] arr){
		
		int sum = 0;
		for (int x: arr){
			sum += x;
		}
		return sum;
	}

	public static void main (String [] args) {
		int [] arr = {21, 120, 77};
		System.out.println(findSum(arr));
	}
}
	