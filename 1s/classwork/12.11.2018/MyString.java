public class MyString {
	
	private char[] chars; //атрибуты строки
	
	public MyString(char [] chars) { 
		this.chars = chars;
	}
	
	public int length() {      
		return chars.length;
	}
	
	public MyString concat(MyString s) {	
		char[] Array = new char[chars.length + s.length];
			for (int i = 0; i < chars.length; i++){
				Array[i] = chars[i];
			}
			for(int i = chars.length; i < Array.length; i++){
				Array[i] = s[i - chars.length];
			}
			return Array;
	}
	
	public char charAt(int i) {		
		return chars[i]; 
		
	}
	
	public void setCharAt(int i, char c) {	
		chars[i] = c;
	}
	
	public int find(MyString s) {...}
	
	public boolean contains(MyString s){
		return find(s) != -1;
	}
	
	public boolean check(MyString re) {...}
	
	public MyString [] split (MyString delimiter) {...}
	
	public static MyString join(MyString [] strs, MyString delimiter) {...}
	
	public boolean equals (MyString s) {
		if (chars.length != s.length()){
			return false;
		}else{
			boolean result = true;
			int i = 0;
			while (result && i < s.length){
				if (s[i] == chars[i]){
					i++;
				}else{
					result = false;
				}
			}
			return result;
		}
		
	}//сравнение строк
	
}
	
	