public class Node<T> {
    T value;
    Node left, right;

    //create leaf node
    public Node(value){
        this.value = value;
        left = right = null;
    }

    public Node(Node left, Node right, T value) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public T getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }
}
