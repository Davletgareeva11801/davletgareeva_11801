public class Subscription {
    private User subscriber;
    private User subscription;

    public Subscription(User subscriber, User subscription){
        this.subscriber = subscriber;
        this.subscription = subscription;
    }

    public User getSubscriber(){
        return subscriber;
    }

    public User getSubscription(){
        return subscription;
    }

    public String toString(){
        return subscriber.getUsername() + ", " + subscription.getUsername();
    }

}