import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {
        ArrayList<User> users = new ArrayList<>();
        ArrayList<Message> messages = new ArrayList<>();
        ArrayList<Subscription> subscriptions = new ArrayList<>();
        Scanner sc1 = new Scanner(new File("users.txt"));
        Scanner sc2 = new Scanner(new File("messages.txt"));
        Scanner sc3 = new Scanner(new File("subscriptions.txt"));

        while (sc1.hasNextLine()) {
            String[] newUser = sc1.nextLine().split("\t");
            users.add(new User(Integer.parseInt(newUser[0]), newUser[1], Integer.parseInt(newUser[2]), newUser[3]));
        }

        while (sc2.hasNextLine()) {
            String[] newMessage = sc2.nextLine().split("\t");
            messages.add(new Message(findUserById(Integer.parseInt(newMessage[0]), users), findUserById(Integer.parseInt(newMessage[1]),users),
                    newMessage[2], new Date(Long.parseLong(newMessage[3]))));
        }

        while (sc3.hasNextLine()) {
            String[] newSubscriptions = sc3.nextLine().split("\t");
            subscriptions.add(new Subscription(findUserById(Integer.parseInt(newSubscriptions[0]), users),
                    findUserById(Integer.parseInt(newSubscriptions[1]), users)));
        }
        for (User x : users) {
            System.out.println(x);
        }

        users.stream();
        Collections.sort(users);
        System.out.println(users);

        Collections.sort(users, new YearComparator());
        System.out.println(users);
        Collections.sort(users, (u1, u2) -> u1.getCity().compareTo(u2.getCity()));
        System.out.println(users);

        double averageYear = users
                .stream()
                .mapToInt(x -> x.getYear())
                .average()
                .orElse(0);
        System.out.println(averageYear);
        int x = Integer.parseInt("10",16);
        System.out.println(x);



    }
    public static User findUserById (int id, ArrayList<User> users) {
        for (User u : users) {
            if ((u.getId() == id)) {
                return u;
            }
        }
        return null;
    }
}
