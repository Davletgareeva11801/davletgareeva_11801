import java.util.ArrayList;
import java.util.stream.Collectors;

public class User implements Comparable<User> {
    private String username;
    private String city;
    private int year;
    private int id;

    public int compareTo(User user) {
        return (username.compareTo(user.getUsername()));
    }

    public User() {
        this.username = null;
        this.year = 0;
        this.city = null;
        this.id = 0;
    }

    public User(int id, String username, int year, String city) {
        this.username = username;
        this.year = year;
        this.city = city;
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public String getCity() {
        return city;
    }

    public String getUsername() {
        return username;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return " id: " + id + " username: " + username + " year: " + year + " city: " + city;
    }

    public boolean equals(User u) {
        return this.id == u.id;
    }

    public ArrayList<User> getSubscribers(ArrayList<Subscription> subscriptions) {
        ArrayList<User> result = new ArrayList<>();
        for (Subscription sub : subscriptions) {
            if (sub.getSubscription().equals(this)) {
                result.add(sub.getSubscriber());
            }
        }
        return result;
    }

    public ArrayList<User> getSubscribersStream(ArrayList<Subscription> subscriptions) {
        ArrayList<User> result = subscriptions.stream()
                .filter(x -> x.getSubscription().equals(this))
                .collect(Collectors.toList())
    }

    public boolean isFriend(User u, ArrayList<Subscription> subscriptions) {
        ArrayList<User> subscribers = this.getSubscribers(subscriptions);
        for (User subscriber : subscribers) {
            if (subscriber.equals(u)) {
                return true;
            }
        }
        return false;


    }
}
