import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {
    private String text;
    private User sender;
    private User receiver;
    private Date date;

    public Message() {
        this.date = null;
        this.receiver = null;
        this.sender = null;
        this.text = null;
    }

    public Message(User sender, User receiver, String text, Date date) {
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public User getReceiver() {
        return receiver;
    }

    public User getSender() {
        return sender;
    }

    public String toString() {
        return "sender: " + sender.getUsername() + "," + "receiver: " + receiver.getUsername() + " , " + text + "Date: " + date;
    }

    public String getText() {
        return text;
    }


}


