import java.util.Comparator;

public class YearComparator implements Comparator<User> {

    @Override
    public int compare(User u1, User u2){
        return (u1.getYear() - u2.getYear());
    }



}
