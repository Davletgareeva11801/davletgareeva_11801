import java.util.Scanner;
public class Main {
	
	public static int findMax(Elem head) {
		int max = head.value;
		Elem p = head.next;
		while (p != null) {
			if (p.value > max) {
				max = p.value;
			}
		}
		return max;
	}
	
	public static Elem deleteFirst(Elem h) {
		return h.next;
	}
		
	public static Elem deleteLast(Elem h) {
		Elem q = h;
		if (q.next == null) {
			return null;
		}
		while(q.next.next != null){
			q = q.next;
		}
		q.next = null;
		return h;
	}
	
	public static Elem deletePenultimate(Elem h) {
		Elem q = h;
		if (q.next == null) {
			return h;
		}
		if (q.next.next == null) {
			return h.next;
		}
		while(q.next.next.next != null) {
			q = q.next;
		}
		q.next = null;
		return h;
	}
	
	public static Elem deleteK(Elem h, int k) {
		Elem p = h;
		while (p != null) {
			if ((p.value == k)&&(p.next == null)) {
				p.value = null;
			} 
			if (p.next == k) {
				(p.next = p.next.next);
			} else {
				return p.next;
			}
		}
		return h;
	}
		
	public static Elem addMAfterK(Elem h, int m, Elem p) {
		Elem q = new Elem();
		q.value = m;
		q.next = p.next;
		p.next = q;
	}
	
	public static Elem addMBeforeK(Elem h, int m, Elem p) {
		Elem q = new Elem();
		q.value = m;
		q.next = p.next;
		p.next = q;
		q.value = p.value;
		p.value = m;
	}
		
	
	public static boolean hasK(Elem head, int k) {
		Elem p = head;
		while (p != null) {
			if(p.value == k) {
				return true;
			}
			p = p.next;
		}
		return false;
	}
			
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		
		Elem p = null;
		Elem head = null;
		int size = 4;
		for(int i = 0; i < size; i++) {
			p = new Elem();
			p.value = in.nextInt();
			p.next = head;
			head = p;
		}
			
		System.out.println(findMax(head));
		
	}
}
		