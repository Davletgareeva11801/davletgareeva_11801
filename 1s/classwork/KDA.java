public class KDA {

	private char[] alphabet;
	private int[] states;
	private int[] finStates;
	private int[][] transition;
	
	public void setAlphabet(char[] a) {
	    alphabet = a;
	}
	
	public void setStates(int[] a) {
		states = a;
	}
	
	public void setFinStates(int[] a) {
		finStates = a;
	}
	
	public void setTransition(int[][] a) {
		transition = a;
	}
	
	public boolean process(String input) {
		int state = 0;
		for(int i = 0; i < input.length(); i++) {
			int c = Integer.parseInt(input.charAt(i));
			state = transition[state][c];
		}
		for (int i = 0; i < finStates.length; i++) {
			if(finStates[i] == state) return true;
		}
	}
}