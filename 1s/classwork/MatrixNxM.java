class MatrixNxM {
	private double n, m;
	private double[][] matrix;
	
	public Matrix2x2() {
		
		for(int i = 0; i < this.n; i++) {
            for(int j = 0; j < this.m; j++) {
                this.matrix[i][j] = 0;
            }
        }
    }
	
	public Matrix2x2(double number) {
		
		for(int i = 0; i < this.n; i++) {
            for(int j = 0; j < this.m; j++) {
                this.matrix[i][j] = number;
            }
        }
    }
	
	public Matrix2x2(double [][] inputMatrix) {
		
		for(int i = 0; i < this.n; i++) {
            for(int j = 0; j < this.m; j++) {
                this.matrix[i][j] = inputMatrix[i][j];
            }
        }
    }
	
	public Matrix2x2(double a, double b, double c, double d) {
	}
	
	public int getHorizontalLength() {
		return this.matrix[0].length;
	}
	
	public int getVerticalLength() {
		return this.matrix.length;
	}
	
	public Matrix2x2 add(Matrix2x2 second) throws NotEqualLengthsOfMatrixException {
		if (this.getHorizontalLength() != second.getHorizontalLength())
			||(this.getVerticalLength() != second.getVerticalLength()) {
				throw  new NotEqualLengthsOfMatrixException();
			} else {
				Matrix resultMatrix = new Matrix(first.getVerticalLength(), second.getHorizontalLength());
            for (int i = 0; i < resultMatrix.getHorizontalLength(); i++) {
                for(int j = 0; j < resultMatrix.getVerticalLength(); j++){
                    resultMatrix[i][j] = this[i][j] + second[i][j];
                }
            }
            return resultMatrix;    
        }
				
		
		
		