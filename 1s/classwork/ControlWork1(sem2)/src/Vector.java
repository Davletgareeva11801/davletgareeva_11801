public class Vector {
    private int x, y;

    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public Vector() {
        this.(0, 0);
    }

    public static int getX(){
        return this.x;
    }
    public static int gety(){
        return this.y;
    }

}
