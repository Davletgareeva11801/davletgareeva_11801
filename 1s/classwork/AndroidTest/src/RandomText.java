import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomText {
    public static String getText(String filename) {
        List<String> wordsList = new ArrayList<>();

        try{
            BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
            String read;
            while ((read = br.readLine()) != null){
                wordsList.add(read);
            }
        }catch(FileNotFoundException e){
            System.out.println("File not found.");
        }catch(IOException e){
            System.out.println("I/O error.");
        }

        Collections.shuffle(wordsList);
        String text = wordsList.get(0);
        return text;

    }

}



