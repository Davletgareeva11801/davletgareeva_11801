import java.io.*;
import java.util.*;

public class Main {
	public static ArrayList<User> users;
	
	public static String findUserById(int id) {
		for(int i = 0; i < users.size(); i++) {
			if (users.get(i).getId() == id) {
				return users.get(i).getUser();
			}
		}
	return null;
	}
				
	public static void main (String[] args) 
		throws IOException {
			Scanner scan0 = new Scanner(new File("Users.txt"));
			users = new ArrayList<>();
			while(scan0.hasNextLine()){
				String[] data = scan0.nextLine().split("\t");
				User au = new User(Integer.parseInt(data[0]), data[1]);
				users.add(au);
			}
			
		Scanner scan1 = new Scanner (new File("Video.txt"));
		ArrayList<Video> videos = new ArrayList<>();
		
		while (scan1.hasNextLine()) {
			String[] data = scan1.nextLine().split("\t");
			Video v = new Video(Integer.parseInt(data[0]), Integer.parseInt(data[1]), Short.parseShort(data[2]),findUserById(Integer.parseInt(data[3])),data[4],Integer.parseInt(data[5]),Integer.parseInt(data[6]));
			videos.add(v);
		}
		System.out.println(videos);
	}
}