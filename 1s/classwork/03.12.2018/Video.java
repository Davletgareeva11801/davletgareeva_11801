public class Video {
	private int likes;
	private int repost;
	private short quality;
	private String userName;
	private String name;
	private int timeInSec;
	private int views;
	
	public Video(int likes, int repost, short quality, String userName, String name, int timeInSec, int views){
		this.likes = likes;
		this.repost = repost;
		this.quality = quality;
		this.userName = userName;
		this.name = name;
		this.timeInSec = timeInSec;
		this.views = views;
	}

	public int getLikes() {
		return likes;
	}
	
	public int getRepost() {
		return repost;
	}
	
	public short getQuality() {
		return quality;
	}
	
	public String getUser() {
		return userName;
	}
	
	public String getName() {
		return name;
	}
	
	public int getTimeInSec() {
		return timeInSec;
	}
	
	public int getViews() {
		return views;
	}
	
	public void setLikes(int likes) {
		this.likes = likes;
	}
	
	public void setRepost(int repost) {
		this.repost = repost;
	}
	
	public void setQuality(short quality) {
		this.quality = quality;
	}
	
	public void setUser(String userName) {
		this.userName = userName;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setTimeInSec(int timeInSec) {
		this.timeInSec = timeInSec;
	}
	
	public void setViews(int views) {
		this.views = views;
	}
	
	public String toString() {
        return likes + " " + repost + " " + quality + " " + userName + " " + name + " " + timeInSec + " " + views;
    }
}