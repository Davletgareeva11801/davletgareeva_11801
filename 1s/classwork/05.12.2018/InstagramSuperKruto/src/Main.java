import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import ru.itis.kpfu.*;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("locations.txt"));
        ArrayList<Location> locations = new ArrayList<Location>();
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            Location location = new Location(temp[0], Double.parseDouble(temp[1]),
                    Double.parseDouble(temp[2]));
            locations.add(location);
        }
        sc = new Scanner(new File("users.txt"));
        ArrayList<User> users = new ArrayList<User>();
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            User user = new User(temp[0], temp[1], temp[2], temp[3], temp[4],
                    temp[5], temp[6], temp[7].equals("m"), temp[8]);
            users.add(user);
        }
        sc = new Scanner(new File("posts.txt"));
        ArrayList<Post> posts = new ArrayList<Post>();
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            Post post = new Post(Integer.parseInt(temp[0]), null, temp[2],
                    temp[3], null, temp[6]);
            for (User user: users) {
                if (user.getName().equals(temp[1])) {
                    post.setWhoUpload(user);
                    break;
                }
            }
           for (Location location: locations) {
                if (Math.abs(location.getX() - Double.parseDouble(temp[4])) <= 0.000001
                        && location.getY() - Double.parseDouble(temp[5]) <= 0.000001) {
                    post.setLocation(location);
                    break;
                }
            }
            posts.add(post);
        }
        sc = new Scanner(new File("followings.txt"));
        ArrayList<Following> followings = new ArrayList<>();
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            Following following = new Following();
            for (User user: users) {
                if (user.getName().equals(temp[0])) {
                    following.setFollower(user);
                    continue;
                }
                if (user.getName().equals(temp[1])) {
                    following.setFollowing(user);
                }
            }
            followings.add(following);
        }
        sc = new Scanner(new File("marks.txt"));
        ArrayList<Mark> marks = new ArrayList<Mark>();
        while(sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            Mark mark = new Mark();
            for (User user : users) {
                if (user.getName().equals(temp[1])) {
                    mark.setUser(user);
                    break;
                }
            }
            for (Post post: posts) {
                if (post.getId() == Integer.parseInt(temp[0])) {
                    mark.setPost(post);
                    break;
                }
            }
            marks.add(mark);
        }
        sc = new Scanner(new File ("likes.txt"));
        ArrayList<Like> likes = new ArrayList<>();
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            Like like = new Like();
            for (User user : users) {
                if (user.getName().equals(temp[0])) {
                    like.setUser(user);
                    break;
                }
            }
            for (Post post: posts) {
                if (post.getId() == Integer.parseInt(temp[1])) {
                    like.setPost(post);
                    break;
                }
            }
            likes.add(like);
        }
        sc = new Scanner(new File("comments.txt"));
        ArrayList<Comment> comments = new ArrayList<>();
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            Comment comment = new Comment(temp[0], null, null);
            for (User user: users) {
                if (user.getName().equals(temp[2])) {
                    comment.setWhoPosted(user);
                    break;
                }
            }
            for (Post post: posts) {
                if (post.getId() == Integer.parseInt(temp[1])) {
                    comment.setPost(post);
                    break;
                }
            }
            comments.add(comment);
        }
    }
}