package ru.itis.kpfu;

import java.util.Objects;

public class Following {
    User follower;
    User following;

    public Following() {
    }

    public User getFollower() {
        return follower;
    }

    public void setFollower(User follower) {
        this.follower = follower;
    }

    public User getFollowing() {
        return following;
    }

    public void setFollowing(User following) {
        this.following = following;
    }

    public Following(User follower, User following) {
        this.follower = follower;
        this.following = following;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Following following1 = (Following) o;
        return Objects.equals(follower, following1.follower) &&
                Objects.equals(following, following1.following);
    }

    @Override
    public int hashCode() {
        return Objects.hash(follower, following);
    }

    @Override
    public String toString() {
        return "Following{" +
                "follower=" + follower +
                ", following=" + following +
                '}';
    }
}
