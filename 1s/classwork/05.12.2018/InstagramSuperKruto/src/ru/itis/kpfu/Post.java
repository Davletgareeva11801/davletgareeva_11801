package ru.itis.kpfu;

import java.util.Date;

public class Post {
    private int id;
    private User whoUpload;
    private String date;
    private String caption;
    private Location location;
    private String photoURL;

    public User getWhoUpload() {
        return whoUpload;
    }

    public void setWhoUpload(User whoUpload) {
        this.whoUpload = whoUpload;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Post(int id, User whoUpload, String date, String caption, Location location, String photoURL) {
        this.id = id;
        this.whoUpload = whoUpload;
        this.date = date;
        this.caption = caption;
        this.location = location;
        this.photoURL = photoURL;
    }


    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", whoUpload=" + whoUpload +
                ", date='" + date + '\'' +
                ", caption='" + caption + '\'' +
                ", location=" + location +
                ", photoURL='" + photoURL + '\'' +
                '}';
    }
}
