package ru.twitter;


import ru.twitter.classesOfTwitter.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

     public static ArrayList<Profile> profiles = new ArrayList<>();
     public static ArrayList<Follower> followers = new ArrayList<>();
     public static ArrayList<Comment> comments = new ArrayList<>();
     public static ArrayList<Tweet> tweets = new ArrayList<>();
     public static ArrayList<Repost> reposts = new ArrayList<>();

     public static void main(String[] args) throws IOException {

        Scanner scanTweet = new Scanner(new File("Tweets.txt"));
        while (scanTweet.hasNextLine()) {
            String[] infTweet = scanTweet.nextLine().split("\\s+");

            ArrayList<Comment> commentsOfTweet = new ArrayList<>();
            for (int i = 0; i < comments.size(); i++) {
                if (comments.get(i).getIdTweetOfComment() == Integer.parseInt(infTweet[0])){
                    commentsOfTweet.add(comments.get(i));
                }
            }
            tweets.add(new Tweet(
                    Integer.parseInt(infTweet[0]),
                    Integer.parseInt(infTweet[1]),
                    infTweet[2], infTweet[3],
                    Integer.parseInt(infTweet[4]),
                    commentsOfTweet
                    )
            );
        }

        Scanner scanFollower = new Scanner(new File("Followers.txt"));
        while (scanFollower.hasNextLine()) {
            String[] infFollower = scanFollower.nextLine().split("\\s+");
            followers.add(new Follower( Integer.parseInt(infFollower[0]), Integer.parseInt(infFollower[1])));
        }

        Scanner scanComment = new Scanner(new File("Comments.txt"));
        while (scanComment.hasNextLine()) {
            String[] infComment = scanComment.nextLine().split("\\s+");

            ArrayList<Comment> commentsOfComment = new ArrayList<>();
            for (int i = 0; i < comments.size(); i++) {
                if (comments.get(i).getIdComment() == Integer.parseInt(infComment[5])){
                    commentsOfComment.add(comments.get(i));
                }
            }
            comments.add(new Comment(
                    Integer.parseInt(infComment[0]),
                    Integer.parseInt(infComment[1]),
                    infComment[2], infComment[3],
                    Integer.parseInt(infComment[4]),
                    commentsOfComment,
                    Integer.parseInt(infComment[5]),
                    Integer.parseInt(infComment[6]),
                    Integer.parseInt(infComment[7])
                    )
            );
        }

        Scanner scanRepost = new Scanner(new File("Reposts.txt"));
        while (scanRepost.hasNextLine()) {
            String[] infRepost = scanRepost.nextLine().split("\\s+");
            reposts.add(new Repost(
                    Integer.parseInt(infRepost[0]),
                    Integer.parseInt(infRepost[1]),
                    Integer.parseInt(infRepost[2])));
        }

         Scanner scanProfile = new Scanner(new File("Profiles.txt"));
         ArrayList<Profile> followingsOfProfile = new ArrayList<>();
         ArrayList<Profile> followersOfProfile = new ArrayList<>();
         while (scanProfile.hasNextLine()) {
             String[] infProfile = scanProfile.nextLine().split("\\s+");

             ArrayList<Tweet> tweetsOfProfile = new ArrayList<>();
             for (int i = 0; i < tweets.size(); i++) {
                 if (tweets.get(i).getProfileID() == Integer.parseInt(infProfile[0])) {
                     tweetsOfProfile.add(tweets.get(i));
                 }
             }

             ArrayList<Comment> commentsOfProfile = new ArrayList<>();
             for (int i = 0; i < comments.size(); i++) {
                 if (comments.get(i).getProfileID() == Integer.parseInt(infProfile[0])) {
                     commentsOfProfile.add(comments.get(i));
                 }
             }

             ArrayList<Repost> repostsOfProfile = new ArrayList<>();
             for (int i = 0; i < reposts.size(); i++) {
                 if (reposts.get(i).getRepostIdProfile() == Integer.parseInt(infProfile[0])) {
                     repostsOfProfile.add(reposts.get(i));
                 }
             }

             profiles.add(new Profile(
                     Integer.parseInt(infProfile[0]),
                     infProfile[1], infProfile[2], infProfile[3],
                     tweetsOfProfile,
                     followingsOfProfile,
                     followersOfProfile,
                     commentsOfProfile,
                     repostsOfProfile
                     )
             );
         }

         for (int i = 0; i < followers.size(); i++) {
             for (int j = 0; j < profiles.size(); j++) {
                 if (followers.get(i).getProfileIdOfFollower() == profiles.get(j).getProfileID()) {
                     for (int k = 0; k < profiles.size(); k++) {
                         if (followers.get(i).getProfileIdOfThatOnFollow() == profiles.get(k).getProfileID()) {
                             profiles.get(k).addFollower(profiles.get(j));
                             profiles.get(j).addFollowing(profiles.get(k));
                         }
                     }
                 }
             }
         }
         for (int i = 0; i < profiles.size(); i++) {
             System.out.println( String.join("\n",  profiles.get(i).toStringList()));

         }






         
    }

}
