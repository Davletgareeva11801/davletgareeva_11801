package ru.twitter.classesOfTwitter;

public class Repost{
	int idProfile;
	int idTweet;
	int idRepost;
	
	public Repost(int idRepost, int idProfile, int idTweet){
		this.idProfile = idProfile;
		this.idTweet = idTweet;
		this.idRepost = idRepost;
	}

	public int getRepostIdProfile(){
		return idProfile;
	}
	public int getReposIdTweet(){
		return idTweet;
	}

	@Override
	public String toString() {

		return "Reposts {" +
				" tweetID = " + idTweet +
				", repostID = '" + idRepost + '\'' +
				" }";
	}
}