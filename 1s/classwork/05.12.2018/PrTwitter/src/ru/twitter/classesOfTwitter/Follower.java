package ru.twitter.classesOfTwitter;

public class Follower {
    private int profileIdOfThatOnFollow;
    private int profileIdOfFollower;

    public Follower(int profileIdOfFollower, int profileIdOfThatOnFollow) {
        this.profileIdOfFollower = profileIdOfFollower;
        this.profileIdOfThatOnFollow = profileIdOfThatOnFollow;
    }


    public int getProfileIdOfFollower() {
        return profileIdOfFollower;
    }

    public int getProfileIdOfThatOnFollow() {
        return profileIdOfThatOnFollow;
    }

    @Override
    public String toString() {

        return "Follower {" +
                "followerID = " + profileIdOfFollower +
                ", profile on which follow" + profileIdOfThatOnFollow +
                " }";
    }
}
