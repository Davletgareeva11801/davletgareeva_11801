public class UsersAudio {

    private String user;
    private String audio;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public UsersAudio(String user, String audio) {
        this.user = user;
        this.audio = audio;
    }
}
