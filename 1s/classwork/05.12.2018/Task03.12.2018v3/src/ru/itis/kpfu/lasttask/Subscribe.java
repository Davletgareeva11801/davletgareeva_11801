package ru.itis.kpfu.lasttask;

public class Subscribe {
    private User user;
    private Channel channel;

    public Subscribe(User user, Channel channel) {
        this.user = user;
        this.channel = channel;
    }

    public User getUser() {
        return user;
    }

    public Channel getChannel() {
        return channel;
    }

    @Override
    public String toString() {
        return "Subscribe{" +
                "user=" + user +
                ", channel=" + channel +
                '}';
    }
}
