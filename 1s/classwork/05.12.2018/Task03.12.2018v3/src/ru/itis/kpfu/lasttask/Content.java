package ru.itis.kpfu.lasttask;

public abstract class Content {
    protected int id;
    protected String name;
    protected Channel channel;
    protected String date;
    protected String info;

    public Content(int id, String name, Channel channel, String date, String info) {
        this.id = id;
        this.name = name;
        this.channel = channel;
        this.date = date;
        this.info = info;
    }

    public String getDate() {
        return date;
    }

    public String getInfo() {
        return info;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Channel getChannel() {
        return channel;
    }

    @Override
    public String toString() {
        return "Video{" +
                "info='" + info + '\'' +
                ", name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", channel=" + channel +
                ", date='" + date + '\'' +
                '}';
    }
}
