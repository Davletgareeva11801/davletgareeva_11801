package ru.itis.kpfu.lasttask;

public class Channel {

    private int channelId;
    private String info;
    private User owner;
    private String name;

    public Channel(int channelId, String info, User owner, String name) {
        this.channelId = channelId;
        this.info = info;
        this.owner = owner;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "channel_id='" + channelId + '\'' +
                ", info='" + info + '\'' +
                ", owner=" + owner +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public int getChannel_id() {
        return channelId;
    }

    public String getInfo() {
        return info;
    }

    public User getOwner() {
        return owner;
    }
}
