package ru.itis.kpfu.lasttask;

public class History {
    private User user;
    private Video video;

    public String getDate() {
        return date;
    }

    private String date;

    public History(User user, Video video, String date) {
        this.user = user;
        this.video = video;
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public Video getVideo() {
        return video;
    }

    @Override
    public String toString() {
        return "History{" +
                "user=" + user +
                ", video=" + video +
                ", date='" + date + '\'' +
                '}';
    }
}
