import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("data.txt"));
        while (sc.hasNextLine()) {
            String[] jar = sc.nextLine().split("\t");
            int n = jar.length;
            LinkedList<Integer> list = new LinkedList<>();
            for (int i = 0; i < n; i++) {
                list.add(Integer.parseInt(jar[i]));
            }
            long c1 = System.currentTimeMillis();
            LinkedListShell.shellSort(list);
            long c2 = System.currentTimeMillis();
            //System.out.println("numberOfElements: " + list.size());
            //System.out.print(list);
            //System.out.println();
            System.out.println(/*"Time:" + */(c2 - c1));
            //System.out.println("\t");
        }
    }

}

