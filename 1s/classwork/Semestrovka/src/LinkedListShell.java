import java.util.LinkedList;

public class LinkedListShell {
    public static void shellSort(LinkedList<Integer> list) {
        int d = list.size()/2;
        //int numberOfIterations = 0;
        while(d > 0) {
            for(int i = 0; i < (list.size() - d); i++){
                int j = i;
                while((j >= 0) && (list.get(j) > list.get(j + d))) {
                    int temp = list.get(j);
                    list.set(j, list.get(j + d));
                    list.set(j + d, temp);
                    j--;
                    //numberOfIterations++;
                }
            }
            //System.out.println("d = " + d);
            d = d/2;
        }
        //System.out.println(/*"numberOfIterations: " + */numberOfIterations);
    }
}