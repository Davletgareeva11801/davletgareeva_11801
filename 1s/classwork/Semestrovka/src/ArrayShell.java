public class ArrayShell {

    public static void shellSort(int[] array) {
        int d = array.length/2;
        int numberOfIterations = 0;
        while(d > 0) {
            for(int i = 0; i < (array.length - d); i++){
                int j = i;
                while((j >= 0) && (array[j] > array[j + d])) {
                    //swap
                    int temp = array[j];
                    array[j] = array[j + d];
                    array[j + d] = temp;
                    j--;
                    numberOfIterations++;
                }
            }
            //System.out.println("d = " + d);
            d = d/2;

        }


        //System.out.println(/*"numberOfIterations: " + */numberOfIterations);

    }
}