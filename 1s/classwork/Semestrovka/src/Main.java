import java.io.File;
//import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("data.txt"));
        //FileWriter writer = new FileWriter("numberOfElements.txt");
        //FileWriter writer1 = new FileWriter("time.txt");
        //FileWriter writer2 = new FileWriter("numberOfIterations.txt");

        while (sc.hasNextLine()) {
            String[] jar = sc.nextLine().split("\t");
            int n = jar.length;
            int[] array = new int[n];
            for (int i = 0; i < n; i++) {
                array[i] = Integer.parseInt(jar[i]);
            }
            long c1 = System.currentTimeMillis();
            ArrayShell.shellSort(array);
            long c2 = System.currentTimeMillis();
            //System.out.println(/*"numberOfElements: " +*/ array.length);
            //writer.write("array.length");
            //System.out.print(Arrays.toString(array));
            //System.out.println();
            System.out.println(/*"Time:" +*/ (c2 - c1));
            //writer1.write((int) (c2 - c1));
            //writer2.write(ArrayShell.shellSort(array));
            //System.out.println("\t");
        }
    }

}