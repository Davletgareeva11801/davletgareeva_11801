import java.io.*;
import java.util.Random;

public class Data {
    private static File file = new File("data.txt");
    public static int[] getArray(int n) {
        int[] array = new int[n];
        Random rm = new Random();
        for(int i = 0; i < array.length; i++) {
            array[i] = rm.nextInt(10000);
        }
        return array;
    }


    public static void toFile(int[] array) {
        try {
            FileWriter writer = new FileWriter("data.txt", 2 == 2);
            for (int i = 0; i < array.length; i++) {
                writer.write(String.valueOf(array[i]));
                writer.write("\t");
            }
            writer.write('\n');
            writer.close();
        } catch (IOException e ) {
            e.printStackTrace();
        }
    }

    public static void clear(File file) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();

    }

    public static void main(String[] args) throws FileNotFoundException {
        Random rm = new Random();
        int n;
        for (int i = 0; i < 50; i++) {
            n = rm.nextInt(10000) + 100;
            toFile(getArray(n));
        }
        //clear(file);

    }
}
