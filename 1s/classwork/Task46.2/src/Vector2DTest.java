import org.junit.Assert;
import org.junit.Test;

public class Vector2DTest {

    Vector2D v = new Vector2D(3,4);
    Vector2D v1 = new Vector2D(1,1);
    Vector2D v2 = new Vector2D(2,3);
    @Test
    public void checkAdd(){
        Assert.assertEquals(v, Vector2D.add(v1, v2));
    }
}
