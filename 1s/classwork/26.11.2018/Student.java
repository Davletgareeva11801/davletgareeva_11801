public class Student {
	
	private String surname;
	private String city;
	private int year;
	private Group group;
	
	public String getSurname() {
		return surname;
	}

	public String getCity() {
		return city;
	}

	public int getYear() {
		return year;
	}

	public Group getGroup() {
		return group;
	}

	public Student(String surname, String city, int year, Group group) {
		this.surname = surname;
		this.city = city;
		this.group = group;
		this.year = year;
	} 
	
	public String toString() {
		return "Surname: " + surname + " City:" + city + " Year:" + year + " Group:" + group ;
	}
}

