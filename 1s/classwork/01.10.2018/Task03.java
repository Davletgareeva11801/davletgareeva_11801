import java.util.Scanner;
// проверить, что в введ. числе есть нечетная цифра 
public class Task03 {
    public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		boolean flag = false;
		while(!flag & n > 0) {
			if ((n % 10) % 2 == 1){
			 flag = true;
			
			}
			 n = n / 10;
		}
		    System.out.println(flag);	
	}
}
		