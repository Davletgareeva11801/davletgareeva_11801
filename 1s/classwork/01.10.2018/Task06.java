// в каждом элементе массива есть цифра, кратная 3 
public class Task06 {
	
	public static void main(String[] args) {
		
		int[] a = {7, 71, 48, 155};
		int n = a.length;
		boolean p = false;
		int i = 0;
		while(!p && i < (n - 1)){
			boolean flag = false;
			while(!flag && a[i] > 0 && i < n){
				if((a[i] % 10) % 3 == 0){
					flag = true;
				}
				a[i] /= 10;
				i++;
			}
		
			i = 0;
			if(!flag){
				p = true;
			}
		}
		System.out.print(p);
	}
}
		