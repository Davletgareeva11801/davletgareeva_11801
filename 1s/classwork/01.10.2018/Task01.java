import java.util.Scanner;
//  проверить сущ.четного эл-та по значению
public class Task01 {
    public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int [] a = {1,11,3,5,5,101,49};
		int n = a.length;
		boolean flag = false;
		int i = 0;
		while (!flag && i < n) {
			if (a[i] % 2 == 0){
				flag = true;
			}
			i++;
		}
		System.out.println(flag) ;
		
	}
}	