import java.io.*;

public class MyThread3 extends Thread {

    private String fileNameR;
    private String fileNameW;
    private int n;

    public MyThread3(String fileNameR, String fileNameW, int n) {
        this.fileNameR = fileNameR;
        this.fileNameW = fileNameW;
        this.n = n;
    }

    public void run() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File(fileNameR)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(fileNameW);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String str = null;
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (str != null) {
            pw.println(str);
            try {
                pw.println(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pw.close();
        }
    }


