public class MyThread2 extends Thread {

    private int[] array;
    private int ind1;
    private int ind2;
    public MyThread2(int[] array, int ind1, int ind2){
        this.array = array;
        this.ind1 = ind1;
        this.ind2 = ind2;
    }
    int sum = 0;
    public void run() {
        for (int i = ind1; i < ind2; i++){
            sum += array[i];
        }
        System.out.println(sum);
    }
}
