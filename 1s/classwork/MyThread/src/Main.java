public class Main {

    public static void main(String[] args) {
        int[] array = {1,2,3,4,5,25,94,1};
        MyThread1 t1 = new MyThread1();
        MyThread1 t2 = new MyThread1();
        MyThread2 firstThread = new MyThread2(array, 0,array.length/2);
        MyThread2 secondThread = new MyThread2(array, array.length/2, array.length);
        MyThread3 t31 = new MyThread3("str.txt", "fout.txt", 5);
        firstThread.start();
        secondThread.start();

        //for (int i = 0; i < 5; i++) {
            t31.start();
            try {
                t31.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
