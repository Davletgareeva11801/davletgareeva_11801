public class Vertex {
    String label;
    boolean isVisited;

    public Vertex(String label){
        this.label = label;
        this.isVisited = false;
    }

    public String getLabel(){
        return label;
    }

    public boolean isVisited(){
        return isVisited;
    }

}
