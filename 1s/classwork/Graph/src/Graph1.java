import java.util.LinkedList;
import java.util.Queue;

public class Graph1 {
    private final int VERTEX_MAX = 100;
    private Vertex[] vertexList;
    private int vertexCount;
    private int[][] matrix;

    public Graph1(){
        matrix = new int[VERTEX_MAX][VERTEX_MAX];
        for(int i = 0; i < VERTEX_MAX; i++)
            for(int j = 0; j < VERTEX_MAX; j++)
                matrix[i][j] = 0;
        vertexCount = 0;
        vertexList = new Vertex[VERTEX_MAX];
    }
    public void addVertex(String label) {
        vertexList[vertexCount++] = new Vertex(label);
    }

    public void addEdge(int begin, int end) {
        matrix[begin][end] = 1;
        matrix[end][begin] = 0;
    }

    //Queue<String> queue = new LinkedList<>();
    //обход в ширину
    public void bfs(int v) {
        Queue<String> queue = new LinkedList<>();
        vertexList[v].isVisited = true;
        queue.insert();
        int vertex;


        //выведем вершину, с которой начинается обход, на экран
        System.out.println(vertexList[v].getLabel());

        while(!queue.isEmpty()){//пока очередь не опустеет
            int current = queue.pop();
            while((vertex = getSuccessor(current)) != -1) {
                vertexList[vertex].isVisited = true;
                queue.insert(vertex);
                //вывод вершины на экран
                System.out.println(vertexList[vertex].getLabel());
            }

        }
        //сброс флагов
        for(int j = 0; j < vertexCount; j++)
            vertexList[j].isVisited = false;
    }
    public int getSuccessor (int v) {
        for(int j = 0; j < vertexCount; j++)
            if(matrix[v][j] == 1 && vertexList[j].isVisited == false)
                return j; //возвращает первую найденную вершину
        return -1; //таких вершин нет
    }
    // обход в глубину
    void dfs(int v){
        vertexList[v].setVisited(true);//алгоритм начинает обход с вершины 0
        stack.push(0);//занесение в стек
        int i = 0;
//выведем вершину, с которой начинается обход, на экран
        System.out,.println(vertexList[v].getLabel());


        while(!stack.isEmpty()) //пока стек не опустеет
        {
            int current = stack.peek();
            //получение непосещенной вершины, смежной с текущей
            int vertex = getSuccessor (current);
            if(vertex == -1)
                stack.pop();//элемент извлекается из стека
            else //если вершина найдена
            {
                vertexList[vertex].setVisited(true);//пометка
                displayVertex(vertex);//вывод
                stack.push(vertex);//занесение в стек
            }
        }

//сброс флагов
        for(int j = 0; j < vertexCount; j++)//сброс флагов
            vertexList[j].setVisited(false);

    }




}
