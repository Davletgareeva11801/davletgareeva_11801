import java.util.ArrayList;

public class Main {

    public static int count =0;
    private static char[] add(char[] a, char x) {
        char[] m = new char[a.length + 1];
        for (int i = 0; i < a.length - 1; i++){
            m[i] = a[i];
        }
        m[a.length] = x;
        return m;
    }

    public static int backTracking(String s ) {
        char a[] = s.toCharArray();
        if (s.length() == 5) {
            count++;
        } else {
            for (char x : a) {
                add(a, x);
            }
            if (correct(s.toCharArray())) {
                a[a.length - 1] = 0;

                backTracking(s);
                //vosstanavlivaem comps there is a[]
            }
            for (char x : a) {
                delete(a, x);
            }
        }
        return count;
    }

    private static void delete(char[] a, char x) {
    }

    private static boolean correct(char[] chars) {
        boolean flag = true;
        for(int j = 0; (j < chars.length)&&flag; j++) {
            if(chars[j+1] - chars[j] > 5){
                flag = false;
            }
        }
        return flag;

    }

    public static void main(String[] args){
        String[] s = {"aaaaa", "abbabb", "agphf" };
        for (int i = 0; i < s.length; i++){
            System.out.println(backTracking(s[i]));
        }

    }
}
