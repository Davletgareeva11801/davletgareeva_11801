import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	
	public static void sortArrayList() { 
		for(int i = people.size() - 1; i > 0; i--) { 
			for(int j = 0; j < i; j++){ 
				if (people.get(j).getSurname().compareToIgnoreCase(people.get(j + 1).getSurname()) > 0){ 
					People p = people.get(j); 
					people.set(j, people.get(j + 1)); 
					people.set(j + 1, p); 
				} 
			} 
		} 
	}

public static ArrayList<People> people = new ArrayList<People>();

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner scan = new Scanner(new File("People.txt"));
		
		while (scan.hasNextLine()) {
			String[] str = scan.nextLine().split("\t");
			String surname = null;
			String email = null;
			String phoneNumber = null;
			
		for (int i = 0; i < str.length; i++) {
			if ((str[i].charAt(0) >= 'A') && (str[i].charAt(0) <= 'Z')) {
				surname = str[i];
			}
			if (str[i].charAt(0) == '+') {
				phoneNumber = str[i];
			}
			for (int j = 0; j < str[i].length(); j++) {
				if (str[i].charAt(j) == '@') {
					email = str[i];
					break;
				}
			}
		}
		People man = new People(surname, email, phoneNumber);
		people.add(man);
		}
	System.out.println(people.toString());
	}
}
