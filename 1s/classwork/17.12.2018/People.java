public class People {
	private String surname;
	private String email;
	private String phoneNumber;
	
	public People (String surname, String email, String phoneNumber) {
		this.surname = surname;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	
	public String getSurname(){
		return surname;
	}
	
	public String getEmail(){
		return email;
	}
	
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String toString() {
		if (phoneNumber == null & email == null) {
			return "(" + "surname " + surname + ")";
		}
		if (email == null) {
			return "(" + "surname " + surname + "phoneNumber " + phoneNumber + ")";
		}
		if (phoneNumber == null) {
			return "(" + "surname " + surname + "email " + email + ")";
		}
		return "(" + "surname " + surname + "phoneNumber " + phoneNumber + "email " + email + ")";
	}
}




	
	
	
	